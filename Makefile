CC = g++

CFLAGS = -std=c++17 -Wall -DBOOST -DLONGLONG_COST -DLONGDOUBLE_PROB 

TB2_PATH = dep/toulbar2

TB2_LIB = -L$(TB2_PATH)/build/lib/Linux
TB2_INCLUDE = -I$(TB2_PATH)/src

# cplex config
ifeq ($(MAKECMDGOALS),main_cplex)
# modify cplex directory if specified in the shell variable
ifneq ($(shell echo $(CPX_LOCATION_TWOPHASES)),)
	CPLEX_LOCATION := $(shell echo ${CPX_LOCATION_TWOPHASES})
else
	CPLEX_LOCATION = $(shell ls -d /opt/ibm/ILOG/CPLEX_Studio*)
endif

CPLEX_LIB = -L $(CPLEX_LOCATION)/cplex/lib/x86-64_linux/static_pic -L $(CPLEX_LOCATION)/concert/lib/x86-64_linux/static_pic
CPLEX_INCLUDE = -I $(CPLEX_LOCATION)/cplex/include -I $(CPLEX_LOCATION)/concert/include
endif

OFLAGS = -O3 -DNDEBUG

SRC_DIR := src
OBJ_DIR := obj
SRC_FILES := $(wildcard $(SRC_DIR)/**/*.cpp | wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))



# conditional rule to compile eighter with or without cplex
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
ifeq ($(MAKECMDGOALS),main_nocplex)
	$(CC) $(CFLAGS) $(OFLAGS) $(TB2_INCLUDE) $^ -c -o $@
else
	$(CC) $(CFLAGS) -DILOGCPLEX $(OFLAGS) $(TB2_INCLUDE) $(CPLEX_INCLUDE) $^ -c -o $@
endif

main_cplex: $(OBJ_FILES)
	$(CC) $(CFLAGS) -DILOGCPLEX $(TB2_INCLUDE) $^ -o main $(TB2_LIB) $(CPLEX_LIB) -ltb2 -lgmp -lboost_graph -lboost_iostreams -lz -llzma -lpthread -ldl -lconcert -lcplex -lilocplex -lm -lpthread -rdynamic -Wl,-rpath=dep/toulbar2/build/lib/Linux

main_nocplex: $(OBJ_FILES)
	$(CC) $(CFLAGS) $(TB2_INCLUDE) $^ -o main $(TB2_LIB) -ltb2 -lgmp -lboost_graph -lboost_iostreams -lz -llzma -lpthread -ldl -lm -lpthread -rdynamic -Wl,-rpath=dep/toulbar2/build/lib/Linux

# -Wl,-rpath= -> https://stackoverflow.com/questions/71108254/how-program-interpreter-locates-the-full-path-of-dynamic-libraries-to-be-loaded

clean:
	@rm $(OBJ_DIR)/*.o 2> /dev/null || true
	@rm main 2> /dev/null || true
