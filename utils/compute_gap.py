import math

# fcosts = 'incomplete_costs.txt'
# flb = 'incomplete_lb.txt'

flb = 'bug_reponses/1dsl_0001_multi_lb.txt'
fcosts = 'bug_reponses/1dsl_0001_multi_costs.txt'

file = open(fcosts, 'r')
lines = file.read().splitlines()
file.close()

costs = []
for line in lines:
  tk = line.split(' ')
  costs.append((float(tk[0]), float(tk[1])))


file = open(flb, 'r')
lines = file.read().splitlines()
file.close()

left = costs[0][0]
right = costs[-1][0]
up = costs[0][1]
down = costs[-1][1]

# ideal
tk = lines[0].split(' ')
ideal = (float(tk[0]), float(tk[1]))
edges = []
for line in lines[1:]:
  tk = line.split(' ')
  edges.append(((float(tk[0]), float(tk[1])), (float(tk[2]), float(tk[3]))))

# initial area
initial_area = (costs[-1][0] - costs[0][0])
initial_area *= (costs[0][1] - costs[-1][1])
print('initial area ', initial_area)



# upper bound area
UB_area = 0.

right = costs[-1][0]
for ind in range(1, len(costs)-1):
  width = right-costs[ind][0]
  height = costs[ind-1][1]-costs[ind][1]
  UB_area += width*height

# lower bound area
LB_area = 0.
for edge in edges:
  if math.fabs(edge[0][0] - edge[1][0]) >= 1e-3:
    LB_area += (edge[1][0] - edge[0][0])*(edge[0][1] - edge[1][1])/2.
  LB_area += (right-edge[1][0])*(edge[0][1]-edge[1][1])

print('UB: ', UB_area)
print('LB: ', LB_area)

print('initial gap: ')

final_gap = LB_area-UB_area
print('final gap: ', final_gap)

relative_gap = final_gap/initial_area
print('relative gap: ', relative_gap*100., '%')