import sys

from matplotlib import pyplot as plt

from matplotlib.patches import Patch
from matplotlib.legend_handler import HandlerTuple
from matplotlib.lines import Line2D

from matplotlib import cm

import numpy as np
import math

# plt.rcParams.update({'font.size': 8})
plt.rcParams.update({'font.size': 8})

print(sys.argv)

# -t
# -c 
# -lb
# -o output filename
# -i : interactive mode
# -t graph title

costs_fname = ''
triangles_fname = ''
lb_fname = ''
output_fname = ''
fig_title = ''

interactive = False

#-----------------------------------------
def show_help():

    # -t
    # -c 
    # -lb
    # -o
    # -i : interactive mode

    print('-t \tname of the file containing the triangles')
    print('-c \tname of the file containing the solution costs')
    print('-lb \tname of the file containing the lower bound edges')
    print('-o \tname of the output graphic file')
    print('-i \toggle interactive mode (show the plot in a window)')
    print('-h \tshow this help')

    return

#-----------------------------------------
def equal(p1, p2):
    return abs(p1[0]-p2[0]) <= 1e-4 and abs(p1[1]-p2[1]) <= 1e-4


index = 1
while index < len(sys.argv):

    if sys.argv[index] == '-t':
        triangles_fname = sys.argv[index+1]
        index += 1
    
    if sys.argv[index] == '-c':
        costs_fname = sys.argv[index+1]
        index += 1
    
    if sys.argv[index] == '-lb':
        lb_fname = sys.argv[index+1]
        index += 1
    
    if sys.argv[index] == '-o':
        output_fname = sys.argv[index+1]
        index += 1

    if sys.argv[index] == '-n':
        fig_title = sys.argv[index+1]
        index += 1

    if sys.argv[index] == '-i':
        interactive = True

    if sys.argv[index] == '-h':
        show_help()

    index += 1

if not interactive and len(output_fname) == 0:
    print('Error, no output filename specified')
    exit()

sol_costs = []
sol_weights = []
sol_lbs = []

pareto_costs = []
file = open(costs_fname, 'r')
lines = file.read().splitlines()
file.close()
for line in lines:
    tokens = line.split(' ')
    pareto_costs.append((float(tokens[0]), float(tokens[1])))

triangles_phase1 = []
if len(triangles_fname) > 0:
    file = open(triangles_fname, 'r')
    lines = file.read().splitlines()
    file.close()
    for line in lines:
        tokens = line.split(' ')
        p1 = (float(tokens[0]), float(tokens[1]))
        p2 = (float(tokens[2]), float(tokens[3]))
        triangles_phase1.append((p1, p2))

file = open(lb_fname, 'r')
lines = file.read().splitlines()
file.close()
tokens = lines[0].split(' ')
lb_ideal = (float(tokens[0]), float(tokens[1]))
lb_edges = []

for line in lines[1:]:
    tokens = line.split(' ')
    edge = ((float(tokens[0]), float(tokens[1])), (float(tokens[2]), float(tokens[3])))
    if edge[0][1] != edge[1][1] or edge[0][1] != edge[1][1]:
        lb_edges.append(edge)

# add aditional horizontal edges to lb if missing
additional_edges = []
for ind in range(len(lb_edges)-1):

    e1 = lb_edges[ind]
    e2 = lb_edges[ind+1]

    if abs(e1[1][1] - e2[0][1]) <= 1e-3:
        if e1[1][0]+1e-3 < e2[0][0]:
            additional_edges.append((e1[1], e2[0]))

lb_edges += additional_edges



supported = []
for cost in pareto_costs:
    supported.append(False)
    for triangle in triangles_phase1:
        v1 = (triangle[1][0]-triangle[0][0], triangle[1][1]-triangle[0][1])
        v2 = (cost[0]-triangle[0][0], cost[1]-triangle[0][1])
        if math.fabs(v1[0]*v2[1] - v1[1]*v2[0]) <= 1e-6:
            supported[-1] = True


fig, ax = plt.subplots()



# triangles from phase 1
# for elt in triangles_phase1:
#     ax.plot([elt[0][0], elt[1][0]], [elt[0][1], elt[0][1]], '--', c='grey', zorder=-1)
#     ax.plot([elt[1][0], elt[1][0]], [elt[0][1], elt[1][1]], '--', c='grey', zorder=-1)
#     ax.plot([elt[0][0], elt[1][0]], [elt[0][1], elt[1][1]], '--', c='grey', zorder=-1)





cmap = cm.get_cmap('viridis')
col_p1 = cmap(0.2)
col_p2 = cmap(0.5)

costs_colors = [col_p2 if not supported[ind] else col_p2 for ind in range(len(pareto_costs))]
ax.scatter([costs[0] for costs in pareto_costs], [costs[1] for costs in pareto_costs], marker='x', color=costs_colors, s=30, linewidth=1, zorder=3)

# ax.scatter([costs[0] for costs in pareto_costs], [costs[1] for costs in pareto_costs], marker='x', color='red', s=20, zorder=-1)

xlim = ax.get_xlim()
ylim = ax.get_ylim()

# update the upper and lower bound sets to add infinite rays

# merge edges if they are on the same line
size = len(lb_edges)-1
ind = 0
while ind < len(lb_edges)-1:
    if abs(lb_edges[ind][0][0] - lb_edges[ind+1][1][0]) <= 1e-4:
        lb_edges[ind] = (lb_edges[ind][0],lb_edges[ind+1][1])
        del lb_edges[ind+1]
        size -= 1
    ind += 1

lb_edges_temp = lb_edges
lb_edges = []

# print(lb_ideal)
# print(lb_edges_temp)

# left ray
lb_edges.append(((lb_ideal[0], ylim[1]), (lb_ideal[0], lb_edges_temp[0][0][1])))

if lb_ideal[0] < lb_edges_temp[0][0][0]:
    lb_edges.append(((lb_ideal[0], lb_edges_temp[0][0][1]), lb_edges_temp[0][0]))

for ind in range(len(lb_edges_temp)):
    lb_edges.append(lb_edges_temp[ind])
    # if ind < len(lb_edges_temp)-1 and not equal(lb_edges_temp[ind][1], lb_edges_temp[ind+1][0]):
    #     lb_edges.append((lb_edges_temp[ind][1], lb_edges_temp[ind+1][0]))

# right ray
if lb_ideal[1] > lb_edges_temp[-1][0][1]:
    lb_edges.append((lb_edges_temp[-1][1], (lb_edges_temp[-1][1][0], lb_ideal[1])))

lb_edges.append(((lb_edges_temp[-1][1][0], lb_ideal[1]), (xlim[1], lb_ideal[1])))

# ub

ub_edges = []
ub_edges.append(((pareto_costs[0][0], ylim[1]),pareto_costs[0]))
for ind in range(len(pareto_costs)-1):
    ub_edges.append(((pareto_costs[ind][0], pareto_costs[ind][1]), (pareto_costs[ind+1][0], pareto_costs[ind][1])))
    ub_edges.append(((pareto_costs[ind+1][0], pareto_costs[ind][1]), (pareto_costs[ind+1][0], pareto_costs[ind+1][1])))
ub_edges.append((pareto_costs[-1], (xlim[1], pareto_costs[-1][1])))

# edges superposition
superposition = []
rem_lb = []
rem_ub = []

for ind1 in range(len(lb_edges)):
    for ind2 in range(len(ub_edges)):

        if equal(lb_edges[ind1][0], ub_edges[ind2][0]) and equal(lb_edges[ind1][1], ub_edges[ind2][1]):
            superposition.append(lb_edges[ind1])
            rem_lb.append(ind1)
            rem_ub.append(ind2)

        elif equal(lb_edges[ind1][0], ub_edges[ind2][0]) and abs(lb_edges[ind1][1][0] - ub_edges[ind2][1][0]) <= 1e-4 and abs(lb_edges[ind1][0][0]-lb_edges[ind1][1][0]) <= 1e-4:
            superposition.append(ub_edges[ind2])
            rem_ub.append(ind2)
            lb_edges[ind1] = (ub_edges[ind2][1], lb_edges[ind1][1])

# clean ub and lb regarding superposing edges            
lb_edges = [lb_edges[ind] for ind in range(len(lb_edges)) if not ind in rem_lb]
ub_edges = [ub_edges[ind] for ind in range(len(ub_edges)) if not ind in rem_ub]


cmap = cm.get_cmap('plasma')
col_lb = cmap(0.2)
col_lbub = cmap(0.5)
col_ub = cmap(0.8)

# draw the lower_bound
for edge in lb_edges:
    ax.plot([edge[0][0], edge[1][0]], [edge[0][1], edge[1][1]], '--', c=col_lb)

# draw the upper bound
for edge in ub_edges:
    ax.plot([edge[0][0], edge[1][0]], [edge[0][1], edge[1][1]], '--', c=col_ub)

# draw the superposition
for edge in superposition:
    ax.plot([edge[0][0], edge[1][0]], [edge[0][1], edge[1][1]], '--', c=col_lbub)



ax.set_xlim(xlim)
ax.set_ylim(ylim)

# ax.set_aspect('equal')
ax.grid()

e0 = Line2D([], [], marker='x', color=col_p2, label='efficient solutions', linestyle='none', markersize=4)
e1 = Line2D([], [], marker='o', color=col_p1, label='efficient solutions', linestyle='none', markersize=4)
e2 = Line2D([], [], marker='o', color=col_p2, label='efficient solutions', linestyle='none', markersize=4)

e3 = Line2D([0], [0], linestyle='--', color=col_lb, label='lower bound')
e4 = Line2D([0], [0], linestyle='--', color=col_ub, label='upper bound')
e5 = Line2D([0], [0], linestyle='--', color=col_lbub, label='lb = ub')

# ax.legend(((e1,e2),e3,e4,e5), ('output solutions','lower bound', 'upper bound', 'lb = ub'), handler_map={tuple: HandlerTuple(ndivide=None, pad=0.2)})

ax.legend((e0,e3,e4,e5), ('output solutions','lower bound', 'upper bound', 'lb = ub'), handler_map={tuple: HandlerTuple(ndivide=None, pad=0.2)})


# ax.legend(handles=legend_elements)

ax.set_xlabel('$F_1$')
ax.set_ylabel('$F_2$')

# start, end = ax.get_ylim()
# ax.yaxis.set_ticks(np.arange(1, 13, 1))

fig.set_tight_layout(True)

ax.set_title(fig_title)

if interactive:
    plt.show()
else:
    fig.savefig(output_fname)