#!/bin/bash

# options -cplex -cplex_dir 

args=( "$@" )

USE_CPLEX=0
CPX_LOCATION=""

for i in $(seq 0 $#);
do
  if [ "${args[i]}" = "-h" ]; then
    echo "usage $0 [-cplex] [-cplex_dir dir]"
    echo "Command options: "
    echo -e " \t -h: show this help"
    echo -e " \t -cplex: include cplex as an optional solver for scalarizations"
    echo -e " \t -cplex_dir dir: specify a custom cplex installation folder, default is /opt/ibm/ILOG/CPLEX_Studio* depending on the cplex version"
    exit 0
  fi
  if [ "${args[i]}" = "-cplex" ]; then
    USE_CPLEX=1
  fi
  if [ "${args[i]}" = "-cplex_dir" ]; then
    CPX_LOCATION=${args[i+1]}
  fi
done

# echo "use_cplex ? "$USE_CPLEX
# echo "CPLEX location: "$CPX_LOCATION

# Dependencies
if [ ! -d dep ]; then
  mkdir dep
fi
cd ./dep/ || exit

# Toulbar2 setup
if [ ! -d toulbar2 ]; then
  echo "cloning toulbar2; master branch"
  git clone -b master https://github.com/toulbar2/toulbar2.git --single-branch
fi

cd toulbar2 || exit

# compilation of toulbar2
mkdir build
cd build || exit

OPT_CPX_1=""
OPT_CPX_2=""
if [ $USE_CPLEX -eq 1 ]; then
  OPT_CPX_1="-DCPLEX=ON"
  if [ "$CPX_LOCATION" != "" ]; then
    OPT_CPX_2="-DCPLEX_LOCATION=${CPX_LOCATION}"
  fi
fi

cmake -DLIBTB2=ON -DCMAKE_BUILD_TYPE=Release "${OPT_CPX_1}" "${OPT_CPX_2}" ..

echo "Compilation of toulbar2"
make -s -j 5

cd ./../../../

# remove the cplex option if required
if [ $USE_CPLEX -eq 1 ]; then
  # modify the cplex options for the bi-objective solver
  if [ "$CPX_LOCATION" != "" ]; then
    export CPX_LOCATION_TWOPHASES=$CPX_LOCATION
  fi
fi

# Compilation
if [ ! -d "obj" ]; then
  mkdir obj
fi

echo "Compilation of tb2_twophases"

if [ $USE_CPLEX -eq 0 ]; then
  make main_nocplex
else
  make main_cplex
fi

# Extraction of the instances
cd instances || exit
tar -zxf instances.tar.gz

# proteins instances
curl -O http://genoweb.toulouse.inra.fr/~degivry/proteins_biobj.zip
unzip -qq proteins_biobj.zip
unxz proteins/*.xz > /dev/null

# Extraction of the benchmark results
cd ../results || exit
tar -zxf expe_cplex.tar.gz
tar -zxf expe_tb2.tar.gz
cd ..
