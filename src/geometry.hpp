#pragma once

#include "two_phases.hpp"

namespace bicriteria {

// normalize weights for line edge definition
void normalize(BiDouble& weights, Double& lb);

// compute the intersection between two lines
BiDouble computeIntersection(Line& line1, Line& line2);

// compute the intersection between a line and an edge
// assume the intersection exists
BiDouble computeIntersection(Line& line, Edge& edge);

// compute the intersection between two edges
// returns double between 0 and 1 for each edge, vectorial representation
BiDouble computeIntersection(Edge& seg1, Edge& seg2);

// compute the intersection between two edges
// the intersection is written in the param intersection
// returns true if the intersection exists
bool computeIntersection(Edge& seg1, Edge& seg2, BiDouble& intersection);

Double dot_product(BiDouble v1, BiDouble v2);

// compute the slope of an edge
Double slope(Edge edge);

// create a vector from an edge
BiDouble make_vector(BiDouble p1, BiDouble p2);

//------------------------------------------------------------------------
bool equal(Double left, Double right);

// return true if two points are equal to each other
bool equal(BiDouble p1, BiDouble p2);

bool equal(Edge e1, Edge e2);


// return true if the point right is on the right or below left
bool right_side(BiDouble& left, BiDouble& right);

//------------------------------------------------------------------------
int dominates(BiDouble left, BiDouble right);

//------------------------------------------------------------------------
int is_dominated(BiDouble left, BiDouble right);


} // namespace bicriteria