#pragma once

#include "two_phases.hpp"

namespace bicriteria {

/*!
 * \class LowerBound
 * \brief store a lower bound set on a pareto set
 */
class LowerBound {

  public:

    /* constructor takes as input a list of solutions that is automatically updated */
    LowerBound(std::vector<std::shared_ptr<bicriteria::Solution>>& psolutions): solutions(psolutions) { }

    /*!
     * \brief compute a lower bound from the first phase
     */
    void computePhase1Lb();

    /*!
     * \brief remove edges of the convex hull when the optimality is proven in phase one
     */
    void filterPhase1(std::vector<std::shared_ptr<bicriteria::Solution>>& solutions, std::vector<std::pair<size_t, size_t>>& triangles_phase1);

    /*!
     * \brief compute edges of the lower bound for the second phase
     */
    void computePhase2Lb();

    /*!
     * \brief compute the lower bound
     */
    void computeLowerBound(std::vector<std::shared_ptr<bicriteria::Solution>>& solutions, std::vector<std::pair<size_t, size_t>>& triangles_phase1);

    /*!
     * \brief compute the area of the dominated points defined by the lower bound
     */
    Double computeLBArea(BiDouble init_area_c1, BiDouble init_area_c2);

  public:

    /* reference to the list of solutions obtained so far */
    std::vector<std::shared_ptr<bicriteria::Solution>>& solutions;

    /* obtained in phase one */
    std::vector<Line> lines_phase1;
    std::vector<Edge> edges_phase1;

    /* edges computed during phase 2 (epsilon constraint method) */
    // one set of edge per non dominated triangle explored
    std::vector<std::vector<Edge>> edges_phase2;

    /* the set of all edges of the lower bound */
    std::vector<Edge> edges_lowerbound;

    /* ideal point: a single point dominating all the pareto solutions */
    BiDouble ideal_costs;
    bool ideal_c1_optimal;
    bool ideal_c2_optimal;

  private:

    //------------------------------------------------------------------
    void project_top(std::vector<Edge>& edges, std::vector<Edge>& frontiere, std::vector<int>& frontiere_ind);

    //------------------------------------------------------------------
    int project_bottom(std::vector<Edge>& edges, std::vector<Edge>& frontiere, std::vector<int>& frontiere_ind);

    //------------------------------------------------------------------
    BiDouble compute_projection(Edge s, BiDouble p);

    //------------------------------------------------------------------
    int find_high_edge(vector<Edge>& edges, Double height_limit, int current_index);

    //------------------------------------------------------------------
    Double slope(Edge s);
    

};


} // namespace twophase