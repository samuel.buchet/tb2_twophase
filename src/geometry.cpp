#include "geometry.hpp"

//------------------------------------------------------------------
Double bicriteria::slope(Edge edge) {
  return (edge.second.first-edge.first.first)/(edge.second.second-edge.first.second);
}

//------------------------------------------------------------------
bool bicriteria::equal(BiDouble p1, BiDouble p2) {
  return fabs(p1.first-p2.first) <= bicriteria::epsilon && fabs(p1.second-p2.second) <= bicriteria::epsilon; 
}

//------------------------------------------------------------------------
bool bicriteria::equal(Double left, Double right) {
  return fabs(left-right) <= bicriteria::epsilon; 
}

//------------------------------------------------------------------------
bool bicriteria::equal(Edge e1, Edge e2) {
  return equal(e1.first, e2.first) && equal(e1.second, e2.second);
}

//-------------------------------------------------------------------------------------------
Double bicriteria::dot_product(BiDouble v1, BiDouble v2) {
  return v1.first*v2.second - v2.first*v1.second;
}

//-------------------------------------------------------------------------------------------
bicriteria::BiDouble bicriteria::make_vector(BiDouble p1, BiDouble p2) {
  return make_pair(p2.first-p1.first, p2.second-p1.second);
}

//--------------------------------------------------------------------------
bicriteria::BiDouble bicriteria::computeIntersection(Edge& seg1, Edge& seg2) {

  // pre: the segments are not colinear

  BiDouble res;

  res.first = (seg1.first.first-seg2.first.first)*(seg2.first.second-seg2.second.second) - (seg1.first.second-seg2.first.second)*(seg2.first.first-seg2.second.first);
  res.first /= (seg1.first.first-seg1.second.first)*(seg2.first.second-seg2.second.second) - (seg1.first.second-seg1.second.second)*(seg2.first.first-seg2.second.first);

  res.second = (seg1.first.first-seg2.first.first)*(seg1.first.second-seg1.second.second) - (seg1.first.second-seg2.first.second)*(seg1.first.first-seg1.second.first);
  res.second /= (seg1.first.first-seg1.second.first)*(seg2.first.second-seg2.second.second) - (seg1.first.second-seg1.second.second)*(seg2.first.first-seg2.second.first);

  return res;
}


//--------------------------------------------------------------------------
bicriteria::BiDouble bicriteria::computeIntersection(Line& line, Edge& segment) {

  // compute the intersection between a line and a segment

  Line line_2;
  line_2.first = make_pair(segment.first.second-segment.second.second, segment.second.first-segment.first.first); // -b, a
  if(line_2.first.first <= -bicriteria::epsilon) {
    line_2.first = make_pair(-line_2.first.first, -line_2.first.second);
  }

  line_2.second = segment.first.first*line_2.first.first + segment.first.second*line_2.first.second;

  return computeIntersection(line, line_2);

}

//--------------------------------------------------------------------------
bool bicriteria::computeIntersection(Edge& seg1, Edge& seg2, BiDouble& intersection) {
  
  BiDouble p1=seg1.first, p2=seg1.second, p3=seg2.first, p4=seg2.second;

  Double denom1 = (p1.first-p2.first)*(p3.second-p4.second) - (p1.second-p2.second)*(p3.first-p4.first);
  Double denom2 = (p1.first-p2.first)*(p3.second-p4.second) - (p1.second-p2.second)*(p3.first-p4.first);

  if(fabs(denom1) <= bicriteria::epsilon || fabs(denom2) <= bicriteria::epsilon) {
    return false;
  }

  Double t1 = (p1.first-p3.first)*(p3.second-p4.second) - (p1.second-p3.second)*(p3.first-p4.first);
  t1 /= denom1; 

  Double t2 = (p1.first-p3.first)*(p1.second-p2.second) - (p1.second-p3.second)*(p1.first-p2.first);
  t2 /= denom2;

  if(t1 >= 0. && t1 <= 1. && t2 >= 0. && t2 <= 1) {
    intersection = make_pair(p1.first+t1*(p2.first-p1.first), p1.second+t1*(p2.second-p1.second)); 
    return true;
  } else {
    return false;
  }

}

//--------------------------------------------------------------------------
bicriteria::BiDouble bicriteria::computeIntersection(bicriteria::Line& line1, bicriteria::Line& line2) {

  bicriteria::BiDouble res;

  // x = (b1*c2 - b2*c1) / (a1*b2 - a2*b1)
  // y = (a2*c1 - a1*c2) / (a1*b2 - a2*b1)

  Double denom = (line2.first.first*line1.first.second - line1.first.first*line2.first.second);

  res.first = (line1.first.second*line2.second - line2.first.second*line1.second);
  res.first /= denom;

  res.second = (line2.first.first*line1.second - line1.first.first*line2.second);
  res.second /= denom;

  return res;

}

//--------------------------------------------------------------------------
void bicriteria::normalize(BiDouble& weights, Double& lb) {

  BiDouble weights_original = weights;

  Double length = sqrt(weights.first*weights.first+weights.second*weights.second);
  
  weights.first /= length;
  weights.second /= length;

  if(fabs(weights_original.first) >= fabs(weights_original.second)) {
    lb *= weights.second/weights_original.second;
  } else {
    lb *= weights.first/weights_original.first;
  }

}

//--------------------------------------------------------------------------
bool bicriteria::right_side(BiDouble& left, BiDouble& right) {

  // return true if right is on the right or aligned and below left

  bool equal_c1 = (fabs(left.first-right.first) <= bicriteria::epsilon);
  bool equal_c2 = (fabs(left.second-right.second) <= bicriteria::epsilon);

  if(equal_c1 && equal_c2) {
    return false;
  }

  if(equal_c1 || right.first - left.first >= bicriteria::epsilon) {
    if(equal_c2 || left.second - right.second >= bicriteria::epsilon) {
      return true;
    }
  }

  return false;

}

//------------------------------------------------------------------------
int bicriteria::dominates(bicriteria::BiDouble left, bicriteria::BiDouble right) {
  
  int c1, c2;
  
  if(fabs(left.first-right.first) <= bicriteria::epsilon) {
    c1 = 0;
  } else if(left.first <= right.first-bicriteria::epsilon) {
    c1 = 1;
  } else {
    c1 = -1;
  }

  if(fabs(left.second-right.second) <= bicriteria::epsilon) {
    c2 = 0;
  } else if(left.second <= right.second-bicriteria::epsilon) {
    c2 = 1;
  } else {
    c2 = -1;
  }

  if(c1 == 1 && c2 == 1) {
    return 2;
  } else if(c1+c2 == 1) {
    return 1;
  } else {
    return 0;
  }

}

//------------------------------------------------------------------------
int bicriteria::is_dominated(bicriteria::BiDouble left, bicriteria::BiDouble right) {
  return bicriteria::dominates(right, left);
}