#pragma once

#include <string>
#include <iostream>
#include <cstdio>

/*----------------------------------------------------------------*/
class Parameters {

  public:

    /*--------------------------------------*/
    Parameters() {

      // two-phases method
      combine = false;
      timeout_phase1 = 30;
      timeout_phase2 = 30;
      global_timeout = 3600;
      n_solve_limit = 1000;
      knapsack = false;
      bisupport = 0.;
      all_solutions = false;
      vac = false;

      // cplex
      use_cplex = false;
      cplex_direct = false;

      // epsilon method
      use_epsilon = false;

      always_combine = false;

    }

    /*--------------------------------------*/
    /* returns false if one parameter is uknown */
    bool parse(int argc, char* argv[]) {

      // -cfn1 filename
      // -cfn2 filename
      // -t timeout solve
      // -m maximum nb call to solve
      // -obj combi/F1 phase 2
      // -knapsack
      // -bisupport
      // 
      // -combine combine the two criteria for phase 2
      

      for(int index = 1; index < argc;) {
          
        std::string token = std::string(argv[index]);
        // std::cout << argv[index] << endl;

        bool test = true;

        if(token == "-cfn1") {
          index ++;
          cfn_1 = argv[index];
          test = false;
        }

        if(token == "-cfn2") {
          index ++;
          cfn_2 = argv[index];
          test = false;
        }

        if(token == "-t") {
          if(index+1 < argc) {
            index ++;
            timeout_phase1 = std::stoi(argv[index]);
            timeout_phase2 = timeout_phase1;
          } else {
            std::cout << "error, no time provided with option -t" << std::endl;
          }
          test = false;
        }

        if(token == "-global_timer") {
          if(index+1 < argc) {
            index ++;
            global_timeout = std::stoi(argv[index]);
          } else {
            std::cout << "error, no time provided with option -global_timer" << std::endl;
          }
          test = false;
        }


        if(token == "-m") {
          index ++;
          n_solve_limit = std::stoi(argv[index]);
          test = false;
        }

        if(token == "-obj") {
          
          index ++;
          std::string opt = std::string(argv[index]);

          if(opt == "combi") {
            combine = true;
          } else if(opt == "F1") {
            combine = false;
          } else {
            std::cout << "uknown value " << opt << " for option -obj" << std::endl; 
          }
          test = false;
        }

        if(token == "-knapsack") {
          knapsack = true;
          test = false;
        }

        if(token == "-vac") {
          vac = true;
          test = false;
        }

        if(token == "-bisupport") {

          index ++;
          std::string opt = std::string(argv[index]);
          
          try {
            bisupport = stof(opt);
          } catch(const std::exception & e) {
            index --;
            bisupport = 0.;
          }

          test = false;
        }

        if(token == "-bisupport:") {
          bisupport = 0.;
          test = false;
        }

        if(token == "-v") {
          index ++;
          std::string opt = std::string(argv[index]);
          verbose = stoi(opt);
          test = false;
        }

        if(token == "-fcosts") {
          index ++;
          costs_fname = std::string(argv[index]); 
          test = false;
        }

        if(token == "-flb") {
          index ++;
          lb_fname = std::string(argv[index]);
          test = false;
        }

        if(token == "-ftriangles") {
          index ++;
          triangles_fname = std::string(argv[index]);
          test = false;
        }

        if(token == "-fsol") {
          index ++;
          sol_fname = std::string(argv[index]);
          test = false;
        }

        if(token == "-epsilon") {
          use_epsilon = true;
          test = false;
        }

        if(token == "-allsol") {
          all_solutions = true;
          test = false;
        }

        if(token == "-solver") {
          index ++;
          std::string solver_name = std::string(argv[index]);
          test = false;
          if(solver_name == "toulbar2") {
            use_cplex = false;
          } else if(solver_name == "cplex") {
            use_cplex = true;
          } else {
            test = true;
          }
        }

        if(token == "-cplex_encoding") {
          index ++;
          std::string enc = std::string(argv[index]);
          test = false;
          if(enc == "direct") {
            cplex_direct = true;
          } else if(enc == "tuple") {
            cplex_direct = false;
          } else {
            test = true;
          }
        }

        if(test) {
          std::cerr << "error option " << token << std::endl;
          return false;
        }

        index ++;
      }

      return true;
    }

  public:


    static int verbose;


    // parameters

    // epsilon method
    bool use_epsilon;

    bool use_cplex; // true if cplex should be used for solving scalarizations
    bool cplex_direct; // true if the direct encodign should be used for cplex, if false, the tuple encoding will be used

    int n_solve_limit; // max number of call to solve allowed
    int timeout_phase1; // timeout for solutions in phase 1
    int timeout_phase2; // timeout for solutions in phase 1
    int global_timeout; // timeout for the complete method

    std::string cfn_1; // filename of the first cfn
    std::string cfn_2; // filename of the second cfn

    bool combine; // if true, combine the two cfn when applying phase 2
    bool knapsack; // if true, use the knapsack constraint
    float bisupport; // use the bisupport value selection heuristic, see toulbar2
    bool vac; // use Virtual Arc Consistency in toulbar2

    bool all_solutions; // if true, all solutions corresponding to the pareto bi costs are enumerated

    std::string costs_fname; // name of the file which will contain the costs
    std::string triangles_fname; // name of the file containing the triangles proved in phase 1
    std::string lb_fname; // name of the file containing the ideal point and the lower bound edges
    std::string sol_fname; // name of the file containing the solutions

    // always combine the two wcsp's (hard) cost functions even if only the first criterion is optimized
    bool always_combine;

};
