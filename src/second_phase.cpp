#include "two_phases.hpp"
#include "lower_bound.hpp"
#include "solution.hpp"

#include "utils.hpp"

#include "toulbar2lib.hpp"
#include "core/tb2wcsp.hpp"
#include "core/tb2globalwcsp.hpp"

using namespace std;

// required for vnsOptimum
extern void newsolution(int wcspId, void* solver);

// global interruption of the search
extern bool global_interruption;

//----------------------------------------------------------------------------
void buildMultiCFNSolution(WCSP* wcsp_1, vector<Value>& sol, MultiCFN::Solution& mcfn_sol) {
    for(unsigned int var_ind = 0; var_ind < sol.size(); var_ind ++) {
      string val_name = wcsp_1->getValueName(var_ind, sol[var_ind]);
      string var_name = wcsp_1->getVar(var_ind)->getName();
      mcfn_sol.insert(make_pair(var_name, val_name));
    }
}

//---------------------------------------------------------------------------
void bicriteria::TwoPhases::clean_wcsp(WCSP* wcsp) {

    if(wcsp->getNbNodes() == 0 && wcsp->getSolutionCost() == MAX_COST) {
      ToulBar2::verbose = -1;
      wcsp->setLb(wcsp->getUb()-MIN_COST);
      WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(wcsp->getUb(), wcsp);
      solver->solve();
      delete solver;
    }

    delete wcsp;
}


//---------------------------------------------------------------------------
bool bicriteria::TwoPhases::solve_epsilon(MultiCFN& mcfn1, MultiCFN& mcfn2, MultiCFN& mcfn_total, vector<int>& global_scope, bicriteria::SolutionPhase2& solution) {

    // mcfn_total.print(cout);
    // cout << endl << endl;

  if(_solver == ToulBar2) {
    return solve_epsilon_HBFS(mcfn1, mcfn2, global_scope, solution);
  } else if(_solver == CPLEX) {
    // SolutionPhase2 solCPLEX(solution);
    
    // cout << "debug: " << endl;
    // cout << "bounds c2: " << solution.bounds_c2 << endl;

    // auto res1 = solve_epsilon_CPLEX(mcfn_total, solCPLEX);
    // cout << endl << endl;

    // // debug
    // // cout << "cplex: " << endl;
    // // mcfn_total.outputNetSolutionCosts(1, solCPLEX.values);
    // // cout << endl << endl;

    // auto res2 = solve_epsilon_HBFS(mcfn1, mcfn2, global_scope, solution);
    // cout << endl << endl;

    // cout << "Comparisons costs cplex vs tb2: " << endl;
    // cout << solCPLEX.ub << " vs " << solution.ub << endl;

    // cout << "Comparisons bicosts cplex vs tb2: " << endl;
    // cout << solCPLEX.costs << " vs " << solution.costs << endl;


    // debug
    // cout << "tb2: " << endl;
    // mcfn_total.outputNetSolutionCosts(1, solution.values);


    // cout << "Comparison solutions values: " << endl;
    // cout << "cplex: ";
    // for(auto& elt: solCPLEX.values) {
    //   cout << elt.first << "=" << elt.second << ", ";
    // }
    // cout << endl << endl << "tb2: ";
    // for(auto& elt: solution.values) {
    //   cout << elt.first << "=" << elt.second << ", ";
    // }
    // cout << endl << endl;
    
    // assert((!res1 && !res2) || fabs(solCPLEX.ub -solution.ub) <= 1e-3);

    // assert(!res2 || (solCPLEX.costs.second >= solution.bounds_c2.first && solCPLEX.costs.second <= solution.bounds_c2.second));

    // return res2;

    #ifdef ILOGCPLEX
    return solve_epsilon_CPLEX(mcfn_total, solution);
    #else
    cout << "Error: the program has not been compiled with CPLEX integration, please change the makefile" << endl;
    return false;
    #endif

  } else {
    cerr << "error: solver undefined" << endl;
    exit(0);
  }

  return false;

}

//---------------------------------------------------------------------------
bool bicriteria::TwoPhases::solve_epsilon_HBFS(MultiCFN& mcfn1, MultiCFN& mcfn2, vector<int>& global_scope, bicriteria::SolutionPhase2& solution) {

  n_solve_phase2 ++;

  bool solution_found = false;

  // return true if a solution has been found

  tb2init();

  ToulBar2::newsolution = newsolution;

  if(Parameters::verbose > 0) {
      ToulBar2::verbose = 0;
  } else {
      ToulBar2::verbose = -1;
  }

  if(_param.vac) {
    ToulBar2::vac = 1;
  } else {
    ToulBar2::vac = 0;
  }
  //ToulBar2::constrOrdering = -8;
  //ToulBar2::costThresholdPre = 100;
  //ToulBar2::useRASPS = 1;
  //ToulBar2::RASPSnbBacktracks = 1000;
  //ToulBar2::RASPSreset = true;
  //ToulBar2::preprocessTernaryRPC = 1;
  //ToulBar2::pwc = -1;
  //ToulBar2::hve = std::numeric_limits<tValue>::max();
  //ToulBar2::elimDegree_preprocessing = -8;
  //ToulBar2::elimSpaceMaxMB = 16;
  
  ToulBar2::bisupport = _param.bisupport;

  // solver will contain the main wcsp
  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(MAX_COST);
  WCSP* wcsp_main = dynamic_cast<WCSP*>(solver->getWCSP());
  mcfn1.makeWeightedCSP(wcsp_main);

  Cost c1_ub_cost;
  if(!_param.combine) {
      c1_ub_cost = wcsp_main->DoubletoCost(solution.bounds_c1.second);
  } else { // when two criteria are mixed, the ub becomes the corner of the triangle
      c1_ub_cost = wcsp_main->DoubletoCost(solution.bounds_c1.second*mcfn1.getWeight(0)+solution.bounds_c2.second*mcfn1.getWeight(1));
  }
  // cout << "c1 ub as cost: c1 < " << c1_ub_cost << endl;  
  wcsp_main->setUb(c1_ub_cost);
  // cout << "c1 negcost: " << wcsp_main->getNegativeLb() << endl;
  
  // do not propagate when creating the two problems in order to keep them as they are before converting them into Knapsack when it is possible
  ToulBar2::dumpWCSP = 1;

  WCSP* wcsp_cstplus = dynamic_cast<WCSP*>(WeightedCSP::makeWeightedCSP(MAX_COST));
  WCSP* wcsp_cstminus = dynamic_cast<WCSP*>(WeightedCSP::makeWeightedCSP(MAX_COST));

  mcfn2.setWeight(0, 1.);
  mcfn2.makeWeightedCSP(wcsp_cstplus);

  // Warning: multicfn will point to wcsp_cstminus, cannot extract the solution of wcsp_cstplus from that point
  mcfn2.setWeight(0, -1.);
  mcfn2.makeWeightedCSP(wcsp_cstminus);

  ToulBar2::dumpWCSP = 0;

  // LB and UB as costs (integers)
  Cost lb_c2_int, ub_c2_int;
  lb_c2_int = wcsp_cstplus->DoubletoCost(solution.bounds_c2.first)+UNIT_COST;
  ub_c2_int = wcsp_cstplus->DoubletoCost(solution.bounds_c2.second);

  if(Parameters::verbose >= 2) {
      cout << " c1 < " << c1_ub_cost << endl;
      cout << lb_c2_int << " <= c2 < " << ub_c2_int << endl;

      cout << "wcsp2 negcost: " << wcsp_cstplus->getNegativeLb() << endl;
      cout << "wcsp2 lb: " << wcsp_cstplus->getLb() << endl;
  }
    

  try {

  int cstInd = wcsp_main->postWeightedCSPConstraint(global_scope, wcsp_cstplus, wcsp_cstminus, lb_c2_int, ub_c2_int, _param.knapsack);

  auto wcsp_cstr = dynamic_cast<WeightedCSPConstraint*>(wcsp_main->getCtr(cstInd));

  // this must be done after adding a global constraint
  wcsp_main->sortConstraints();

  // warning cases with weakly dominated solutions should be managed with care
  // when solutions have the same cost on the second criterion

  if(Parameters::verbose >= 2) {
      cout << "solving in the triangle!" << endl;
  }

  // early stop, let's be lucky
  if(!_param.combine) {
      ToulBar2::vnsOptimum = wcsp_main->DoubletoCost(solution.bounds_c1.first);
  } else {
      Double comb = mcfn1.getWeight(0)*solution.bounds_c1.first+mcfn1.getWeight(1)*solution.bounds_c2.first;
      ToulBar2::vnsOptimum = wcsp_main->DoubletoCost(comb);
  }
    

  bool result = false;

  // init toulbar2 timer
  ToulBar2::startCpuTime = cpuTime();
  timer(_param.timeout_phase2);

  // try {
  result = solver->solve();
  // } catch(const TimeOut& ex) {
      // cout << "timeout" << endl;
  //  }

  // if no solution has been found, limited indicates if it is a proof
  solution.optimal = !ToulBar2::limited;
  solution.lb = solver->getDDualBound();

  nb_nodes_phase2 += solver->getNbNodes();
  nb_backtracks_phase2 += solver->getNbBacktracks(); 

  // stop the timer
  timerStop();

  if(result) {

      solution_found = true;

      // correct optimality flag id the search returned known optimum
      if(!solution.optimal && ToulBar2::vnsOptimum == solver->getSolutionCost()) {
        solution.optimal = true;
      }

      // if(!combine) {
          auto sol_tb2 = solver->getSolution();
          buildMultiCFNSolution(wcsp_main, sol_tb2, solution.values);
          WeightedCSPConstraint::unprotect();
          
        //   solution.costs = make_pair(solver->getSolutionValue(), wcsp_cstr->computeSolutionCost(sol_tb2));
          
          // warning, solution.costs contains the bi-costs, not combined
          solution.costs = make_pair(mcfn1.getSolutionValues()[0], wcsp_cstplus->Cost2ADCost(wcsp_cstr->computeSolutionCost(sol_tb2)));

      // } else {
    //   solution.values = mcfn1.getSolution();
    //   auto costs = mcfn1.getSolutionValues();
    //   solution.costs = make_pair(costs[0], costs[1]); // the two cfn are always in mcfn1
      // }
      

      solution.ub = solver->getSolutionValue();
      if(solution.optimal) { // this should be fixed
          solution.lb = solution.ub;
      }

      // c2_ub = wcsp_cstr->computeSolutionCost(sol);

      // check if the solution is on a triangle to correctly set the optimal flag

      // if(wcsp_cstplus->DoubletoCost(solution.ub_c2) - wcsp_cstplus->DoubletoCost(solution.lb_c2) <= UNIT_COST) {
      //     // complete = true;
      //     // the constraint become infeasible
      // }

  }

  // delete wcsp_cstplus;
  // delete wcsp_cstminus;

  clean_wcsp(wcsp_cstplus);
  clean_wcsp(wcsp_cstminus);

  delete solver;

  return solution_found;

  } catch (const Contradiction&) {

    cout << "Contradiction when creating the globalWeightedCSP constraint" << endl;

    solution.optimal = true;
    solution.lb = wcsp_main->Cost2ADCost(c1_ub_cost);

    // since solve() is not called, is it necessary to clean the wcsps ?

  //   delete wcsp_cstplus;
  //   delete wcsp_cstminus;

    clean_wcsp(wcsp_cstplus);
    clean_wcsp(wcsp_cstminus);

    delete solver;
    return false; 
    
  }
}


//------------------------------------------------------------------------------------------------------------------------------------------------
bool bicriteria::TwoPhases::second_phase(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2,  vector<shared_ptr<bicriteria::Solution>>& solutions, vector<pair<size_t, size_t>>& triangles_phase1, bicriteria::LowerBound& pareto_lb) {

    bool exact = true;

    n_solve_phase2 = 0;

    nb_nodes_phase2 = 0;
    nb_backtracks_phase2 = 0; 

    WCSP* wcsp1_ptr = dynamic_cast<WCSP*>(wcsp_1);
    WCSP* wcsp2_ptr = dynamic_cast<WCSP*>(wcsp_2);

    MultiCFN mcfn1, mcfn2;
    mcfn1.push_back(wcsp1_ptr);

    MultiCFN mcfn_total;
    if(_solver == CPLEX) {
        mcfn_total.push_back(wcsp1_ptr);
        mcfn_total.push_back(wcsp2_ptr);
    }

    // when F2 is not optimized, it may be necessary to add hard constraints not duplicated between the wcsp's
    if(_param.always_combine || _param.combine) {
        mcfn1.push_back(wcsp2_ptr, 0.);
    }

    mcfn2.push_back(wcsp2_ptr);

    // scope used by the global wcsp constraint
    vector<int> global_scope;
    init_global_scope(wcsp_1, wcsp_2, global_scope);

    unsigned int tr_ind = 0;

    // iterate over all the triangles to solve them
    auto lb_iterator = pareto_lb.edges_phase2.begin();
    for(auto& edge: triangles_phase1) {
        
        // stop the search if max number of call to solve has been reached
        if(n_solve_phase1 + n_solve_phase2 >= _param.n_solve_limit) {
            exact = false;
            if(Parameters::verbose >= 2) {
                cout << endl << "Interruption! max number of call to solve reach" << endl << endl; 
            }
            break;
        }

        // stop the search if there is a global interruption
        if(global_interruption) {
            if(Parameters::verbose >= 1) {
                cout << "Interrupting phase 2!" << endl;
            }  
            exact = false;
            break;
        }

        // if the two criteria are combined, the weights are the optimal ones from the first phase
        if(_param.combine) {
            mcfn1.setWeight(0, solutions[edge.first]->costs.second-solutions[edge.second]->costs.second);
            mcfn1.setWeight(1, solutions[edge.second]->costs.first-solutions[edge.first]->costs.first);
        }

       if(Parameters::verbose >= 2) {
            cout << endl << endl;
            cout << "current triangle from " << solutions[edge.first]->costs << " and " << solutions[edge.second]->costs << endl;
       }

    //    tr_ind += 1;
    //    if(tr_ind == 10) {
    //     Parameters::verbose = 2;
    //    } else {
    //     Parameters::verbose = 0;
    //    }

        BiDouble bounds_c1 = make_pair(solutions[edge.first]->costs.first, solutions[edge.second]->costs.first);
        BiDouble bounds_c2 = make_pair(solutions[edge.second]->costs.second, solutions[edge.first]->costs.second); 

        assert(lb_iterator->back().first.second > lb_iterator->back().second.second);

        // compute bound costs from the triangle sides

        if(Parameters::verbose >= 2) {
            cout << endl << endl;
            cout << "bounds: c1 <= " << bounds_c1.second << " ; " << bounds_c2.first << " <= c2 <= " << bounds_c2.second << endl;
        }

        // list of triangles to explore
        // lb ub c1, lb ub c2
        list<pair<BiDouble, BiDouble>> pending_bounds;
        pending_bounds.push_back(make_pair(bounds_c1, bounds_c2));

        // only the upper bound updates during the search
        while(!pending_bounds.empty()) {

            // stop the search if max number of call to solve has been reached
            if(n_solve_phase1 + n_solve_phase2 >= _param.n_solve_limit) {
                exact = false;
                if(Parameters::verbose >= 2) {
                    cout << endl << "Interruption! max number of call to solve reach" << endl << endl; 
                }
                break;
            }

            // stop the search if there is a global interruption
            if(global_interruption) {
                if(Parameters::verbose >= 1) {
                    cout << "Interrupting phase 2!" << endl;
                }  
                exact = false;
                break;
            }

            shared_ptr<SolutionPhase2> solution = make_unique<SolutionPhase2>();

            if(_param.combine) {
                solution->weights = make_pair(mcfn1.getWeight(0), mcfn1.getWeight(1));
            } else {
                solution->weights = make_pair(1., 0.);
            }

            solution->bounds_c1 = pending_bounds.front().first;
            solution->bounds_c2 = pending_bounds.front().second;
            pending_bounds.pop_front();

            if(Parameters::verbose >= 2) {
                cout << endl << endl;
                cout << "current bounds: " << solution->bounds_c1.first << " <= c1 < " << solution->bounds_c1.second << endl;
                cout << "\t " << solution->bounds_c2.first << " <= c2 < " << solution->bounds_c2.second << endl;
                if(_param.combine) {
                    cout << "using weights " << solution->weights << endl;
                }
            }

            // precondition for each loop iteration: the bounds come from optimal solutions (feasible upper bounds) 

            // if there is not enough space between the bounds to obtain a solution, the search stops and a new lb edge is added
            if(wcsp2_ptr->DoubletoCost(solution->bounds_c2.second)-wcsp2_ptr->DoubletoCost(solution->bounds_c2.first) <= UNIT_COST) {
                lb_iterator->push_back(make_pair(make_pair(solution->bounds_c1.second, solution->bounds_c2.second), make_pair(solution->bounds_c1.second, solution->bounds_c2.first)));
                assert(lb_iterator->back().first.second > lb_iterator->back().second.second);
                continue;
            }


            bool result = solve_epsilon(mcfn1, mcfn2, mcfn_total, global_scope, *solution);

            // if the solution is not optimal, it is discared
            if(result) {

                if(!solution->optimal) {
                    exact = false;
                }

                if(Parameters::verbose >= 2) {
                    cout << "new point in phase 2: " << solution->costs << endl;
                    cout << "optimality ? " << solution->optimal << endl;
                }

                // add the new lb edge to the list
                pair<BiDouble, BiDouble> lb_edge;
                if(!_param.combine) {

                    lb_edge.first = make_pair(solution->lb, solution->bounds_c2.second);
                    lb_edge.second = make_pair(solution->lb, solution->bounds_c2.first);

                    // cout << "sol lb vs sol bounds c1: " << solution->lb << " vs " << solution->bounds_c1.first << endl;

                    // if(!solution->optimal || solution->lb > solution->bounds_c1.first+bicriteria::epsilon) {
                    
                    lb_iterator->push_back(lb_edge);
                    //     cout << "adding the edge " << lb_iterator->back() << endl;
                    // }

                    
                    


                    assert(lb_iterator->back().first.second > lb_iterator->back().second.second);
                
                } else {
                    // special case when one side of the triangle is a part of the lower bound 
                    if(solution->lb > solution->weights.first*solution->bounds_c1.first+solution->weights.second*solution->bounds_c2.second) {
                        
                        Double left = (solution->lb-solution->weights.second*solution->bounds_c2.second)/solution->weights.first;
                        Double bottom = (solution->lb-solution->weights.first*solution->bounds_c1.second)/solution->weights.second;

                        // non vertical edge cut out on the triangle sides
                        lb_edge.first = make_pair(left, solution->bounds_c2.second);
                        lb_edge.second = make_pair(solution->bounds_c1.second, bottom);
                        lb_iterator->push_back(lb_edge);
                        assert(lb_iterator->back().first.second > lb_iterator->back().second.second);


                        // vertical edge for the cut part
                        lb_edge.first = make_pair(solution->bounds_c1.second, bottom);
                        lb_edge.second = make_pair(solution->bounds_c1.second, solution->bounds_c2.first);
                        lb_iterator->push_back(lb_edge);
                        if(Parameters::verbose >= 3) {
                            cout << "last edge: " << lb_edge << endl;
                        }
                        assert(lb_iterator->back().first.second > lb_iterator->back().second.second);

                    } else {
                        lb_edge.first = make_pair((solution->lb-solution->weights.second*solution->bounds_c2.second)/solution->weights.first, solution->bounds_c2.second);
                        lb_edge.second = make_pair((solution->lb-solution->weights.second*solution->bounds_c2.first)/solution->weights.first, solution->bounds_c2.first);
                        lb_iterator->push_back(lb_edge);
                        assert(lb_iterator->back().first.second > lb_iterator->back().second.second);

                        if(Parameters::verbose >= 2) {
                            cout << "new edge " << lb_iterator->back() << endl;
                        }

                    }

                    
                }


                // add the new solution to the list if it is not dominated
                // this condition could be true only when combining the two criteria
                if(!_param.combine || wcsp2_ptr->DoubletoCost(solution->costs.first) < wcsp2_ptr->DoubletoCost(solution->bounds_c1.second)) {
                    solutions.push_back(solution);
                }

                
                if(solution->optimal) {  // if the solution is not proved optimal, the search stops

                    // updating the bounds and adding a new sub problem

                    if(!_param.combine) {
                        auto new_bounds_c1 = make_pair(max(solution->bounds_c1.first, solution->lb), solution->bounds_c1.second);
                        auto new_bounds_c2 = make_pair(solution->bounds_c2.first, solution->costs.second);
                        pending_bounds.push_back(make_pair(new_bounds_c1, new_bounds_c2));
                    } else {

                        Double left = (solution->lb-solution->weights.second*solution->bounds_c2.second)/solution->weights.first;
                        Double bottom = (solution->lb-solution->weights.first*solution->bounds_c1.second)/solution->weights.second;

                        // only an upper triangle should be built
                        if(solution->costs.first > solution->bounds_c1.second) {
                            
                            auto new_bounds_c1 = make_pair(left, solution->bounds_c1.second);
                            auto new_bounds_c2 = make_pair(bottom, solution->bounds_c2.second);
                            pending_bounds.push_back(make_pair(new_bounds_c1, new_bounds_c2));

                        } else {
                            // upper triangle
                            auto new_bounds_c1 = make_pair(left, solution->costs.first);
                            auto new_bounds_c2 = make_pair(solution->costs.second, solution->bounds_c2.second);
                            pending_bounds.push_back(make_pair(new_bounds_c1, new_bounds_c2));
                        
                            // lower triangle
                            new_bounds_c1 = make_pair(solution->costs.first, solution->bounds_c1.second);
                            new_bounds_c2 = make_pair(solution->bounds_c2.first, solution->costs.second);
                            pending_bounds.push_back(make_pair(new_bounds_c1, new_bounds_c2));
                        }
                    }
                    

                }

                /* check the solution with multicfn */
                // MultiCFN::Solution mcfn_sol;
                // buildMultiCFNSolution(wcsp_main, sol, mcfn_sol);
                // cout << "solution in mcfn format: " << endl;
                // for(auto& elt: mcfn_sol) {
                //     cout << elt.first << "=" << elt.second << ", ";
                // }
                // cout << endl;
                // MultiCFN debug_mcfn;
                // debug_mcfn.push_back(dynamic_cast<WCSP*>(wcsp_1));
                // debug_mcfn.push_back(dynamic_cast<WCSP*>(wcsp_2));
                // auto costs = debug_mcfn.computeSolutionValues(mcfn_sol);
                // cout << "costs: " << costs << endl;

            }
            
            // if no solution, add the corresponding edge (necessary for the combination only ?)
            if(!result) {

                // the search ends here if there is no solution: either there is no solution or the solution computed is dominated

                if(Parameters::verbose >= 2) {
                    cout << "No solution " << endl;
                    cout << "optimality ? " << solution->optimal << endl;
                }

                // no solution means there is no point in the triangle formed by the two solutions
                if(solution->optimal && solutions[edge.second]->optimal && solution->optimal) {
                    lb_iterator->push_back(make_pair(make_pair(solution->bounds_c1.second, solution->bounds_c2.second), make_pair(solution->bounds_c1.second, solution->bounds_c2.first)));
                    assert(lb_iterator->back().first.second > lb_iterator->back().second.second);
                }

                if(!solution->optimal) {
                    exact = false;
                }
                
            }

        }

        lb_iterator ++;
    }



    return exact;
}

