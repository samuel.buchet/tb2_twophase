#include <iostream>
#include <csignal>
#include <mutex>

#include "two_phases.hpp"
#include "epsilon_method.hpp"
#include "lower_bound.hpp"
#include "parameters.hpp"
#include "utils.hpp"

using namespace std;

int Parameters::verbose = 0;

std::mutex interruption_mutex;
bool global_interruption {};

namespace { volatile std::sig_atomic_t gSignalStatus; }

//-------------------------------------------------------------------------
void signal_handler(int signal) {
  
  gSignalStatus = signal;

  cout << "User interruption signal received" << endl;

  std::lock_guard<std::mutex> guard(interruption_mutex);
  global_interruption = true;

  bicriteria::TwoPhases* solver = bicriteria::TwoPhases::getInstance();
  if(solver != nullptr) {
    solver->abort();
  }

}

//---------------------------------------------------------------------------
int main(int argc, char* argv[]) {

  // install a signal handler
  std::signal(SIGINT, signal_handler);

  // global interruption variable
  {
    std::lock_guard<std::mutex> guard(interruption_mutex);
    global_interruption = false;
  }

  srand(42);

  if(argc < 1) {
    return 0;
  }

  Parameters param;

  if(!param.parse(argc, argv)) {
    cerr << "Error while parsing the parameters!" << endl;
    return -1;
  }
  if(param.cfn_1.rfind(".") != string::npos && param.cfn_2.rfind(".") != string::npos && param.cfn_1.substr(param.cfn_1.rfind(".")) != param.cfn_2.substr(param.cfn_2.rfind("."))) {
    cerr << "Error: both objective files must have the same format extension!" << endl;
    return -1;
  } else if(param.cfn_1.rfind(".") != string::npos && param.cfn_2.rfind(".") != string::npos) {
    string s1 = param.cfn_1.substr(0,param.cfn_1.rfind("."));
    string s2 = param.cfn_2.substr(0,param.cfn_2.rfind("."));
    if(s1.rfind(".") != string::npos && s2.rfind(".") != string::npos && s1.substr(s1.rfind(".")) != s2.substr(s2.rfind("."))) {
      cerr << "Error: both objective compressed files must have the same format extension!" << endl;
      return -1;
    }
  }

  #ifndef ILOGCPLEX
  if(param.use_cplex) {
    cout << "Error: the program has not been compiled with CPLEX integration, please change the install options!" << endl;
    return 0;
  }
  #endif

  if(!param.use_epsilon) {
    bicriteria::TwoPhases two_phases_solver(param);
    two_phases_solver.two_phases();
  } else {
    EpsilonMethod solver(param);
    solver.epsilon_method();
  }
  
  return 0;
}
