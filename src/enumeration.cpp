#include "two_phases.hpp"
#include "solution.hpp"
#include "utils.hpp"

#include "core/tb2wcsp.hpp"
#include "core/tb2globalwcsp.hpp"

#include <memory>

using namespace std;

// required for vnsOptimum
extern void newsolution(int wcspId, void* solver);

//------------------------------------------------------------------------
void bicriteria::TwoPhases::enumerate_solutions(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2, vector<std::shared_ptr<bicriteria::Solution>>& solutions, vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase1, vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase2) {

  // mcfn's for phase 1
  MultiCFN mcfn;
  mcfn.push_back(dynamic_cast<WCSP*>(wcsp_1));
  mcfn.push_back(dynamic_cast<WCSP*>(wcsp_2));

  // mcfn's for phase 2
  MultiCFN mcfn1, mcfn2;
  mcfn1.push_back(dynamic_cast<WCSP*>(wcsp_1), 1.);
  mcfn1.push_back(dynamic_cast<WCSP*>(wcsp_2), 0.); // when F2 is not optimized, it is necessary to add potential hard cosntraints

  mcfn2.push_back(dynamic_cast<WCSP*>(wcsp_2));

  // scope used by the global wcsp constraint for phase 2 enumeration
  vector<int> global_scope;
  init_global_scope(wcsp_1, wcsp_2, global_scope);

  for(auto& solution: solutions) {
    if(solution->getType() == Solution::Solution_Phase1) {
      enum_sol_phase1(mcfn, solution, all_solutions_phase1);
    } else {
      enum_sol_phase2(mcfn1, mcfn2, global_scope, solution, all_solutions_phase2);
    }
  }

}

//------------------------------------------------------------------------
void bicriteria::TwoPhases::enum_sol_phase1(MultiCFN& mcfn, std::shared_ptr<bicriteria::Solution>& solution, vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase1) {

  if(Parameters::verbose >= 1) {
    cout << endl << endl;
    cout << "Enumeration from the bi-costs " << solution->costs << " from Phase 1" << endl;
  }

  bicriteria::SolutionPhase1* sol = dynamic_cast<SolutionPhase1*>(solution.get());

  if(Parameters::verbose >= 2) {
    cout << "current solution: " << solution->costs << endl;
    cout << "weights: " << sol->weights << endl;
    cout << "ub: " << sol->ub << endl;
  }

  /* solve the scalarization again */
  mcfn.setWeight(0, sol->weights.first);
  mcfn.setWeight(1, sol->weights.second);

  tb2init();


  if(_param.vac) {
    ToulBar2::vac = 1;
  } else {
    ToulBar2::vac = 0;
  }

  if(Parameters::verbose > 0) {
    ToulBar2::verbose = 0;
  } else {
    ToulBar2::verbose = -1;
  }

  // compute all solutions
  ToulBar2::allSolutions = 1000;

  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(MAX_COST);

  WCSP* wcsp = dynamic_cast<WCSP*>(solver->getWCSP());
  mcfn.makeWeightedCSP(wcsp);

  // ub correponds to the optimal cost found for the solution, + 1
  wcsp->setUb(wcsp->DoubletoCost(sol->ub)+UNIT_COST);

  // solver->getWCSP()->dump_CFN(cout);

  // start the timer
  ToulBar2::startCpuTime = cpuTime();
  timer(_param.timeout_phase1);

  bool res = false;

  try {
    res = solver->solve();
  } catch(const TimeOut& ex) {
    if(Parameters::verbose >= 3) {
      cout << "timeout" << endl;
    }
  }

  // stop the timer
  timerStop();

  // nb_nodes_phase1 += solver->getNbNodes();
  // nb_backtracks_phase1 += solver->getNbBacktracks(); 

  solution->optimal = !ToulBar2::limited;
  solution->lb = solver->getDDualBound();

  assert(res);

  if(Parameters::verbose >= 2) {
    cout << "n solutions found: " << solver->getSolutions().size() << endl;
  }

  if(Parameters::verbose >= 2 && solver->getSolutions().size() > 0) {
    cout << "ub: " << solver->getSolutions().front().first << " vs " << solution->ub << endl;
  }

  /* for each new solution, build a corresponding mcfn solution to recover the costs and variable values*/
  for(auto& cost_sol: solver->getSolutions()) {

    MultiCFN::Solution mcfn_sol = mcfn.convertToSolution(cost_sol.second);
    
    std::vector<Double> bicosts = mcfn.computeSolutionValues(mcfn_sol);

    shared_ptr<bicriteria::SolutionPhase1> newsol_phase1 = make_unique<bicriteria::SolutionPhase1>();

    newsol_phase1->weights = sol->weights;
    newsol_phase1->values = mcfn_sol;
    newsol_phase1->costs = make_pair(bicosts[0], bicosts[1]);
    newsol_phase1->optimal = true;

    newsol_phase1->lb = cost_sol.first;
    newsol_phase1->ub = cost_sol.first;

    all_solutions_phase1.push_back(newsol_phase1);

  }

  delete solver;



}

//------------------------------------------------------------------------
void bicriteria::TwoPhases::enum_sol_phase2(MultiCFN& mcfn1, MultiCFN& mcfn2, std::vector<int>& global_scope, std::shared_ptr<bicriteria::Solution>& solution, vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase2) {

  if(Parameters::verbose >= 1) {
    cout << endl << endl;
    cout << "Enumeration from the bi-costs " << solution->costs << " from Phase 2" << endl;
  }

  assert(solution->getType() == Solution::Solution_Phase2);

  bicriteria::SolutionPhase2* sol = dynamic_cast<SolutionPhase2*>(solution.get());

  if(Parameters::verbose >= 2) {
    cout << "current solution: " << solution->costs << endl;
    cout << "weights: " << sol->weights << endl;
    cout << "ub: " << sol->ub << endl;
  }

  /* solve contrained problem again to enumerate all the solutions */

  if(_param.combine) {
    mcfn1.setWeight(0, sol->weights.first);
    mcfn1.setWeight(1, sol->weights.second);
  }

  tb2init();

  ToulBar2::newsolution = newsolution;

  if(_param.vac) {
    ToulBar2::vac = 1;
  } else {
    ToulBar2::vac = 0;
  }
  ToulBar2::bisupport = _param.bisupport;

  if(Parameters::verbose > 0) {
    ToulBar2::verbose = 0;
  } else {
    ToulBar2::verbose = -1;
  }

  // compute all solutions
  ToulBar2::allSolutions = 1000;

  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(MAX_COST);

  WCSP* wcsp_main = dynamic_cast<WCSP*>(solver->getWCSP());
  mcfn1.makeWeightedCSP(wcsp_main);

  // ub correponds to the optimal cost found for the solution, + 1
  wcsp_main->setUb(wcsp_main->DoubletoCost(solution->ub)+UNIT_COST);

  // set up the global constraint
  WCSP* wcsp_cstplus = dynamic_cast<WCSP*>(WeightedCSP::makeWeightedCSP(MAX_COST));
  WCSP* wcsp_cstminus = dynamic_cast<WCSP*>(WeightedCSP::makeWeightedCSP(MAX_COST));

  mcfn2.setWeight(0, 1.);
  mcfn2.makeWeightedCSP(wcsp_cstplus);

  // Warning: multicfn will point to wcsp_cstminus, cannot extract the solution of wcsp_cstplus from that point
  mcfn2.setWeight(0, -1.);
  mcfn2.makeWeightedCSP(wcsp_cstminus);

  // LB and UB as costs (integers)
  Cost lb_c2_int, ub_c2_int;
  lb_c2_int = wcsp_cstplus->DoubletoCost(solution->costs.second);
  ub_c2_int = wcsp_cstplus->DoubletoCost(solution->costs.second)+UNIT_COST;

  try {

  // creation of the global constraint
  int cstInd = wcsp_main->postWeightedCSPConstraint(global_scope, wcsp_cstplus, wcsp_cstminus, lb_c2_int, ub_c2_int, _param.knapsack);

  auto wcsp_cstr = dynamic_cast<WeightedCSPConstraint*>(wcsp_main->getCtr(cstInd));

  // solver->getWCSP()->dump_CFN(cout);

  // start the timer
  ToulBar2::startCpuTime = cpuTime();
  timer(_param.timeout_phase1);

  bool res = false;

  try {
    res = solver->solve();
  } catch(const TimeOut& ex) {
    if(Parameters::verbose >= 3) {
      cout << "timeout" << endl;
    }
  }

  // stop the timer
  timerStop();

  assert(res);

  if(Parameters::verbose >= 2) {
    cout << "n solutions found: " << solver->getSolutions().size() << endl;
  }

  if(Parameters::verbose >= 2 && solver->getSolutions().size() > 0) {
    cout << "ub: " << solver->getSolutions().front().first << " vs " << solution->ub << endl;
  }

  /* for each new solution, build a corresponding mcfn solution to recover the costs and variable values*/
  for(auto& cost_sol: solver->getSolutions()) {

    MultiCFN::Solution mcfn_sol = mcfn1.convertToSolution(cost_sol.second);
    
    std::vector<Double> bicosts = mcfn1.computeSolutionValues(mcfn_sol);

    shared_ptr<bicriteria::SolutionPhase2> newsol_phase2 = make_unique<bicriteria::SolutionPhase2>();

    newsol_phase2->weights = sol->weights;
    newsol_phase2->values = mcfn_sol;
    newsol_phase2->costs = make_pair(bicosts[0], bicosts[1]);
    newsol_phase2->optimal = true;

    newsol_phase2->lb = cost_sol.first;
    newsol_phase2->ub = cost_sol.first;

    all_solutions_phase2.push_back(newsol_phase2);

  }

  clean_wcsp(wcsp_cstplus);
  clean_wcsp(wcsp_cstminus);

  delete solver;

  } catch (const Contradiction&) {

    cerr << "Error: Contradiction when with the globalWeightedCSP constraint when enumerating the solutions in phase 2" << endl;

    clean_wcsp(wcsp_cstplus);
    clean_wcsp(wcsp_cstminus);

    delete solver;
    
  }


}