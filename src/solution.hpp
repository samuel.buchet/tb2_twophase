#pragma once

#include "two_phases.hpp"

namespace bicriteria {

/*!
 * \class Solution
 * \brief represent a solution of a bi-objective wcsp
 */
class Solution {

  public:

    enum SolutionType { Solution_Default, Solution_Phase1, Solution_Phase2, Solution_Epsilon };

  public:
    Solution() { }
    virtual SolutionType getType() { return Solution_Default; }
    virtual void dumpType(std::ostream& os) { os << "Solution_Default"; }

  public:

    bool optimal; /* true if the solution is optimal */
    MultiCFN::Solution values;
    BiDouble costs; /* costs of the two criteria */
    BiDouble weights; /* weights used on the solution if relevant, null otherwise */

    Double lb, ub; /* lower and upper bounds returned by the solver, tight only if the optimality is proven */

};


/*!
 * \class Solution
 * \brief represent a solution of the first phase
 */
class SolutionPhase1: public Solution {

  public:
    SolutionPhase1(): Solution() { }
    virtual SolutionType getType() { return Solution::Solution_Phase1; }
    virtual void dumpType(std::ostream& os) { os << "Solution_Phase1"; }

  public:
    
    Double ub_c1; // initial upper bound on the cfn

};


/*!
 * \class SolutionPhase2
 * \brief represent a solution of the second phase
 */
class SolutionPhase2: public Solution {

  public:
    SolutionPhase2(): Solution() { }
    virtual SolutionType getType() { return Solution::Solution_Phase2; }
    virtual void dumpType(std::ostream& os) { os << "Solution_Phase2"; }

  public:
    
    BiDouble bounds_c1; // lower and upper bound on the optimized criteria during the second phase, the lower bound is used as a hint to stop the search
    BiDouble bounds_c2; // lower and upper bound on the criteria used as a as constraint 

};

/*!
 * \class SolutionEpsilon
 * \brief represent a solution of the epsilon constraint method
 */
class SolutionEpsilon: public Solution {

  public:
    SolutionEpsilon(): Solution() { }
    virtual SolutionType getType() { return Solution::Solution_Epsilon; }
    virtual void dumpType(std::ostream& os) { os << "Solution_Epsilon"; }

  public:
    
    Double ub_c1; // upper bound on the optimized criteria during the second phase, the lower bound is used as a hint to stop the search
    Double ub_c2; // lower and upper bound on the criteria used as a as constraint 

};




} // namespace bicriteria