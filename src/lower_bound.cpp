#include "lower_bound.hpp"

#include "two_phases.hpp"
#include "solution.hpp"
#include "geometry.hpp"

#include <algorithm>


using namespace std;


//--------------------------------------------------------------------------
void bicriteria::LowerBound::computePhase1Lb() {

  vector<Edge> temp_edges;

  // check if the line should be used for the lb segments or not 
  bool dominated = true; // always true for the first iteration

  for(auto& line: lines_phase1) {

    vector<BiDouble> extremities;

    // compute the intersections on the "ideal edges"
    // w1 c1 + w2 c2 = lb
    BiDouble inter_left = make_pair(ideal_costs.first, (line.second-ideal_costs.first*line.first.first)/line.first.second);
    BiDouble inter_bottom = make_pair((line.second-ideal_costs.second*line.first.second)/line.first.first, ideal_costs.second);

    Double dot = -1;

    // iterate over the edges and figure out which one dominates or is intersected
    for(auto it = temp_edges.begin(); it != temp_edges.end();) {

      Double v1 = (*it).first.first*line.first.first + (*it).first.second*line.first.second;
      Double v2 = (*it).second.first*line.first.first + (*it).second.second*line.first.second;

      int first_side = (line.second-v1 <= bicriteria::epsilon ? 1 : (line.second-v1 >= -bicriteria::epsilon ? -1: 0));
      int second_side = (line.second-v2 <= bicriteria::epsilon ? 1 : (line.second-v2 >= -bicriteria::epsilon ? -1: 0));

      if(first_side == -1 || second_side == -1) {
        dominated = true;
      }

      // if the segment is on the line, it can be removed and will be replaced by the line
      // if(first_side == 0 && second_side == 0) {
      //   dominated = true;
      // }

      // -1 -> the segment side dominates the line


      // if the segment dominates the line, it should be removed (it is a worst lower bound)
      if(first_side+second_side <= -1) {
        it = temp_edges.erase(it);
        continue;
      }

      // if the segment crosses the line, an intersection must be computed      
      if((first_side == 1 && second_side <= 0) || (first_side <= 0 && second_side == 1)) {

        auto inter = computeIntersection(line, *it);

        // store the point as an extremity of the segment
        extremities.push_back(inter);
        if(extremities.size() == 1) {
          BiDouble wtemp = make_pair(it->first.second-it->second.second, it->second.first-it->first.first);
          dot = wtemp.first*line.first.second - wtemp.second*line.first.first;
        }

        // shorten the existing segment into two parts
        if(first_side == 1) {
          it->second = inter;
        } else {
          it->first = inter;
        }

      }
  
      it ++;
      
    }

    // add then new segment
    if(extremities.size() == 2) {
      // ordered
      if(extremities[1].first-extremities[0].first >= bicriteria::epsilon || extremities[0].second-extremities[1].second >= bicriteria::epsilon) {
        temp_edges.push_back(make_pair(extremities[0], extremities[1]));
      } else {
        temp_edges.push_back(make_pair(extremities[1], extremities[0]));
      }
    } else if(extremities.size() == 1) {
      if(dot > 0) {
        temp_edges.push_back(make_pair(extremities[0], inter_bottom));
      } else {
        temp_edges.push_back(make_pair(inter_left, extremities[0]));
      }
    } else if(dominated) { // extremities.size() == 0
      temp_edges.push_back(make_pair(inter_left, inter_bottom));
    }

    // reset
    dominated = false;
    
  }
    
  sort(temp_edges.begin(), temp_edges.end(), [](auto& elt1, auto& elt2) { return min(elt1.first.first, elt1.second.first) < min(elt2.first.first, elt2.second.first); } );


  // filter points
  for(unsigned int index = 0; index < temp_edges.size(); index ++) {
    if(!equal(temp_edges[index].first, temp_edges[index].second)) {
      edges_phase1.push_back(temp_edges[index]);
    }
  }

}

//-------------------------------------------------------------------------------------------
void bicriteria::LowerBound::filterPhase1(vector<shared_ptr<Solution>>& solutions, vector<pair<size_t, size_t>>& triangles_phase1) {

  if(Parameters::verbose >= 3) {
    cout << "filtering phase 1" << endl;
  }

  for(auto& triangle: triangles_phase1) {


    if(Parameters::verbose >= 3) {
      cout << "triangle " << solutions[triangle.first]->costs << " and " << solutions[triangle.second]->costs << endl;
    }
    
    auto it_end = edges_phase1.end();

    for(auto it = edges_phase1.begin(); it != it_end;) {

      if(equal(solutions[triangle.first]->costs, it->first) && equal(solutions[triangle.second]->costs, it->second)) {
        if(Parameters::verbose >= 3) {
          cout << "removing " << *it << endl;
          cout << "because " << solutions[triangle.first]->costs << " and " << solutions[triangle.second]->costs << endl;
        }
        it = edges_phase1.erase(it);
        it_end = edges_phase1.end();
      } else {

        // compute the line equation
        BiDouble w;
        w.first = it->first.second-it->second.second;
        w.second = it->second.first-it->first.first;
        Double lb = it->first.first*w.first+it->first.second*w.second;

        Double v1 = solutions[triangle.first]->costs.first*w.first+solutions[triangle.first]->costs.second*w.second;
        Double v2 = solutions[triangle.second]->costs.first*w.first+solutions[triangle.second]->costs.second*w.second;

        // cut the edge
        if(equal(v1, lb) && equal(v2, lb)) {

          cout << "cutting the edge: " << *it << endl;

          if(equal(solutions[triangle.first]->costs, it->first)) {
            it->first = solutions[triangle.second]->costs;
          } else if(equal(solutions[triangle.second]->costs, it->second)) {
            it->second = solutions[triangle.first]->costs;
          } else {
            Edge new_edge = make_pair(it->first, solutions[triangle.second]->costs);
            it->first = solutions[triangle.second]->costs;
            it = edges_phase1.insert(edges_phase1.end(), new_edge);
            it_end = edges_phase1.end();
          }

        }
        it ++;
        
      }
    }


  }

}





//-------------------------------------------------------------------------------------------
int bicriteria::LowerBound::find_high_edge(vector<bicriteria::Edge>& edges, Double height_limit, int current_index) {

  int best_ind = -1;

  for(unsigned int i = 0; i < edges.size(); i ++) {

    if(current_index == static_cast<int>(i)) {
      continue;
    }

    if(edges[i].first.second >= height_limit) {
      continue;
    }

    if(best_ind == -1) {
      best_ind = i;
      continue;
    }

    bicriteria::Edge s1 = edges[i];
    bicriteria::Edge s2 = edges[best_ind];

    if(fabs(s1.first.second-s2.first.second) <= bicriteria::epsilon && s1.first.first >= s2.first.second) {
      best_ind = i;
    } else if(s1.first.second >= s2.first.second) {
      best_ind = i;
    }

  }

  return best_ind;

}

//------------------------------------------------------------------
bicriteria::BiDouble bicriteria::LowerBound::compute_projection(Edge edge, BiDouble p) {

  Double t = (p.second-edge.first.second)/(edge.second.second-edge.first.second);

  return make_pair(edge.first.first + t*(edge.second.first-edge.first.first), p.second);

}


//------------------------------------------------------------------
void bicriteria::LowerBound::project_top(vector<Edge>& edges, vector<Edge>& frontier, vector<int>& frontier_ind) {

  // int i = 0;

  bool stop = false;
  while(!stop) {

    if(Parameters::verbose >= 3) {
      cout << endl << endl << "project top on " << frontier.back() << endl;
    }

    assert(!equal(frontier.back().first, frontier.back().second));

    // i ++;
    // if(i > 10) {
    //   break;
    // }

    bicriteria::BiDouble v1 = make_vector(frontier.back().first, frontier.back().second);

    // find the highest projection on the current edge, or an intersection
    int best_proj = -1;

    BiDouble best_inter;
    int best_inter_ind = -1;

    for(unsigned int i = 0; i < edges.size(); i ++) {

      if(static_cast<int>(i) == frontier_ind.back()) {
        continue;
      }

      if(equal(edges[i], frontier.back())) {
        continue;
      }

      // first test the intersection
      BiDouble inter;
      bool inter_res = computeIntersection(frontier.back(), edges[i], inter);

      if(inter_res && edges[i].second.second <= inter.second && !equal(inter, frontier.back().first) && !equal(inter, edges[i].second)) {
        
        if(Parameters::verbose >= 3) {
          cout << "intersection with " << edges[i] << endl;
          cout << "intersection computed: " << inter << endl;
        }

        auto v1 = make_vector(frontier.back().first, frontier.back().second);
        auto v2 = make_vector(frontier.back().first, edges[i].second);

        if(Parameters::verbose >= 3) {
          cout << "dot product: " << dot_product(v1, v2) << endl;
        }

        if(dot_product(v1, v2) >= 0) {
          if(best_inter_ind == -1 || best_inter.second <= inter.second) {
            best_inter_ind = i;
            best_inter = inter;
            if(Parameters::verbose >= 3) {
              cout << "changing the best inter so far" << endl;
            }
          }
        }
      }

      // if no intersection, test the projection
      if(edges[i].first.second <= frontier.back().first.second && edges[i].first.second >= frontier.back().second.second) {
        // test if it is dominated
        BiDouble v2 = make_vector(frontier.back().first, edges[i].first);
        
        if(dot_product(v1, v2) >= 0) {
          if(best_proj == -1 || edges[best_proj].first.second <= edges[i].first.second) {
            best_proj = i;
          }
        }
      }
    }

    stop = true;

    // use the projection
    if(best_proj != -1 && (best_inter_ind == -1 || best_inter.second <= edges[best_proj].first.second)) {

      if(Parameters::verbose >= 3) {
        cout << "using the projection" << endl;
      }

      // apply the projection
      BiDouble p = compute_projection(frontier.back(), edges[best_proj].first);
      frontier.back().second = p;
      if(Parameters::verbose >= 3) {
        cout << "modifying frontiere.back() (projection): " << frontier.back() << endl;
      }
      if(equal(frontier.back().first.second, frontier.back().second.second)) {
        frontier.erase(frontier.end()-1);
        frontier_ind.erase(frontier_ind.end()-1);
      }
      if(Parameters::verbose >= 3) {
        cout << "best proj selected: " << edges[best_proj] << endl;
        cout << "p: " << p << endl;
        cout << "edges[best_proj]: " << edges[best_proj].first << endl;
        cout << "vs inter: " << best_inter << endl;
      }

      frontier.push_back(edges[best_proj]);
      frontier_ind.push_back(best_proj);
      
      if(Parameters::verbose >= 3) {
        cout << "new edge in the frontier: " << frontier.back() << endl; 
      }
      
      stop = false;
    }

    // use the intersection
    if(best_inter_ind != -1 && (best_proj == -1 || best_inter.second >= edges[best_proj].first.second)) {

      if(Parameters::verbose >= 3) {
        cout << "using the intersection: " << best_inter << endl;
      }

      frontier.back().second = best_inter;

      if(equal(frontier.back().first.second, frontier.back().second.second)) {
        frontier.erase(frontier.end()-1);
        frontier_ind.erase(frontier_ind.end()-1);
      }

      if(Parameters::verbose >= 3) {
        cout << "modifying frontiere.back(): (intersection)" << frontier.back() << endl;
      }

      frontier.push_back(make_pair(best_inter, edges[best_inter_ind].second));
      if(Parameters::verbose >= 3) {
        cout << "adding " << frontier.back() << " to the frontiere (intersection)" << endl;
      }
      frontier_ind.push_back(best_inter_ind);
      
      if(Parameters::verbose >= 3) {
        cout << "new edge in the frontier: " << frontier.back() << endl; 
      }
      
      stop = false;
    }
  
  }

}

//------------------------------------------------------------------
int bicriteria::LowerBound::project_bottom(vector<Edge>& edges, vector<Edge>& frontier, vector<int>& frontier_ind) {

  if(Parameters::verbose >= 3) {
    cout << "project bottom from " << frontier.back() << endl; 
  }

  int best_proj = -1;
  BiDouble best_proj_p;

  for(unsigned int i = 0; i < edges.size(); i ++) {

    if(static_cast<int>(i) == frontier_ind.back()) {
      continue;
    }
    
    if(frontier.back().second.second <= edges[i].first.second && frontier.back().second.second >= edges[i].second.second) {

      BiDouble p = compute_projection(edges[i], frontier.back().second);

      // make sure we dont project on the bottom of an edge
      if((best_proj == -1 || p.first >= best_proj_p.first) && !equal(p, edges[i].second)) {
        if(Parameters::verbose >= 3) {
          cout << "projection found on " << edges[i] << endl; 
          cout << "projection: " << p << endl;
        }
        best_proj = i;
        best_proj_p = p;
      }
    }

  }

  if(best_proj != -1) {
    frontier.push_back(make_pair(best_proj_p, edges[best_proj].second));
    if(Parameters::verbose >= 3) {
      cout << "adding " << frontier.back() << " to the frontier (from project bottom)" << endl;
    }
    frontier_ind.push_back(best_proj);
  }

  return best_proj;
}

//-------------------------------------------------------------------------------------------
void bicriteria::LowerBound::computePhase2Lb() {

  // debugging tests
  // edges_phase2.push_back(vector<Segment>());
  // ifstream file("random_segments.txt");
  // while(!file.eof()) {
  //   Segment seg;
  //   file >> seg.first.first;
  //   file >> seg.first.second;
  //   file >> seg.second.first;
  //   file >> seg.second.second;
  //   if(!file.eof()) {
  //     edges_phase2.back().push_back(seg);
  //   }
  // }
  // file.close();

  for(auto& triangle: edges_phase2) {

    vector<bicriteria::Edge> frontier;
    vector<int> frontier_indexes;

    // find the edge with the highest highest point
    int high_seg = find_high_edge(triangle, std::numeric_limits<Double>::infinity(), -1);

    if(high_seg == -1) {
      continue;
    }

    frontier_indexes.push_back(high_seg);
    frontier.push_back(triangle[high_seg]);

    bool stop = false;

    while(!stop) {

      project_top(triangle, frontier, frontier_indexes);

      int res_bottom = project_bottom(triangle, frontier, frontier_indexes);

      if(res_bottom == -1) {
        int res = find_high_edge(triangle, frontier.back().second.second, frontier_indexes.back());
        if(res != -1) {
          frontier_indexes.push_back(res);
          frontier.push_back(triangle[res]);
        } else {
          stop = true;
        }
      }
    }

    // debugging tests
    // ofstream file2("output_cpp.txt");
    // for(auto& seg: frontiere) {
    //   file2 << seg.first.first << " " << seg.first.second;
    //   file2 << " " << seg.second.first << " " << seg.second.second;
    //   file2 << endl;
    // }
    // file2.close();

    triangle = frontier;

  }

  

}

//-------------------------------------------------------------------------------------------
void bicriteria::LowerBound::computeLowerBound(vector<shared_ptr<bicriteria::Solution>>& solutions, vector<pair<size_t, size_t>>& triangles_phase1) {

  computePhase1Lb();
  
  // ofstream file;
  // file.open("edges_phase1.txt");
  // for(auto& elt: edges_phase1) {
  //   file << elt.first.first << " " << elt.first.second << " " << elt.second.first << " " << elt.second.second << endl;
  // }
  // file.close();

  filterPhase1(solutions, triangles_phase1);
  
  computePhase2Lb();

  edges_lowerbound.insert(edges_lowerbound.end(), edges_phase1.begin(), edges_phase1.end());
  for(auto triangle: edges_phase2) {
    edges_lowerbound.insert(edges_lowerbound.end(), triangle.begin(), triangle.end());
  }
  sort(edges_lowerbound.begin(), edges_lowerbound.end(), [](bicriteria::Edge e1, bicriteria::Edge e2) { return e1.first.second > e2.first.second; } );

}

//-------------------------------------------------------------------------------------------
Double bicriteria::LowerBound::computeLBArea(BiDouble init_area_c1, BiDouble init_area_c2) {

  Double area = 0.;

  for(auto it = edges_lowerbound.begin(); it != edges_lowerbound.end(); it ++) {
    
    // triangle
    if(fabs(it->first.first-it->second.first) >= bicriteria::epsilon) {
      area += fabs(it->first.first-it->second.first)*(it->first.second-it->second.second)/2.;
    }
    
    // rectangle
    area += (init_area_c1.second-max(it->first.first, it->second.first))*(it->first.second-it->second.second);
  }

  // extremities

  assert(edges_lowerbound.size() > 0);

  // top
  area += (init_area_c1.second-ideal_costs.first) * (init_area_c2.second-edges_lowerbound.front().first.second);

  return area;
}