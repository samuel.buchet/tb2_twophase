
#include "toulbar2lib.hpp"
#include "core/tb2wcsp.hpp"
#include "core/tb2globalwcsp.hpp"

#include "epsilon_method.hpp"
#include "utils.hpp"
#include "geometry.hpp"

using namespace std;

//------------------------------------------------------------------
EpsilonMethod::EpsilonMethod(Parameters& param): _param(param) {



}


//------------------------------------------------------------------
void EpsilonMethod::epsilon_method() {

  tb2init();
  initCosts();

  if(Parameters::verbose > 0) {
    ToulBar2::verbose = 0;
  } else {
    ToulBar2::verbose = -1;
  }

  set_tb2_extflags(_param.cfn_1.c_str());

  WeightedCSP* wcsp1 = nullptr;
  WeightedCSP* wcsp2 = nullptr;

  cout << "c1: " << _param.cfn_1 << " ; c2: " << _param.cfn_2 << endl;

  // do not propagate when reading the two problems in order to keep them as they are before converting into MultiCFN 
  ToulBar2::dumpWCSP = 1;

  /* first criterion */
  wcsp1 = WeightedCSP::makeWeightedCSP(MAX_COST);
  wcsp1->read_wcsp(_param.cfn_1.c_str());
  wcsp1->sortConstraints();

  /* second criterion */
  wcsp2 = WeightedCSP::makeWeightedCSP(MAX_COST);
  wcsp2->read_wcsp(_param.cfn_2.c_str());
  wcsp2->sortConstraints();

  ToulBar2::dumpWCSP = 0;

  // run the epsilon-contraint method
  solve(wcsp1, wcsp2);


  /* dummy solve call to make sure memory is cleared */
  ToulBar2::verbose = -1;
  wcsp1->setLb(wcsp1->getUb()-MIN_COST);
  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(wcsp1->getUb(), wcsp1);
  solver->solve();
  delete solver;

  wcsp2->setLb(wcsp2->getUb()-MIN_COST);
  solver = WeightedCSPSolver::makeWeightedCSPSolver(wcsp2->getUb(), wcsp2);
  solver->solve();
  delete solver;


  delete wcsp1;
  delete wcsp2;

}

//------------------------------------------------------------------
void EpsilonMethod::solve(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2) {

  auto t_start_total = std::chrono::high_resolution_clock::now();

  nb_backtracks = 0;

  vector<int> global_scope;
  init_global_scope(wcsp_1, wcsp_2, global_scope);

  vector<bicriteria::SolutionEpsilon> solutions;
  vector<bicriteria::Edge> lb_edges;
  bicriteria::BiDouble ideal_costs;


  // optimize separately on each criteria
  MultiCFN mcfn12, mcfn2;
  mcfn12.push_back(dynamic_cast<WCSP*>(wcsp_1), 1.);
  mcfn12.push_back(dynamic_cast<WCSP*>(wcsp_2), 0.);

  mcfn2.push_back(dynamic_cast<WCSP*>(wcsp_2), 1.);

  bicriteria::SolutionEpsilon sol_c1, sol_c2;
  
  bool res1 = solve_mono(mcfn12, sol_c1);

  cout << endl << endl;


  mcfn12.setWeight(0, 0.);
  mcfn12.setWeight(1, 1.);
  bool res2 = solve_mono(mcfn12, sol_c2);
  cout << endl << endl;

  ideal_costs.first = sol_c1.costs.first;
  ideal_costs.second = sol_c2.costs.second;

  solutions.push_back(sol_c1);

  cout << "initial bounds: " << sol_c1.costs << " ; " << sol_c2.costs << endl;

  bool stop = false;

  bicriteria::SolutionEpsilon sol;

  if(!res1 || !res2) {
    stop = true;
  } else {
    sol.ub_c2 = sol_c1.costs.second;
    sol.ub_c1 = sol_c2.costs.first;
  }
  
  mcfn12.setWeight(0, 1.);
  mcfn12.setWeight(1, 0.);

  while(!stop) {

    cout << endl << endl;
    cout << "current bound: c2 < " << sol.ub_c2 << endl;

    bool res = solve_bounds(mcfn12, mcfn2, global_scope, sol);
    
    if(res) {
      cout << "new solution: " << sol.costs << endl;
      // stopping criteria: the optimal solution on c1 has been reach
      if(bicriteria::equal(sol_c2.costs.second, sol.costs.second)) {
        stop = true;
      } else {
        solutions.push_back(sol);
        bicriteria::Edge new_edge;
        new_edge.first = make_pair(sol.costs.first, sol.ub_c2);
        new_edge.second = sol.costs;
        lb_edges.push_back(new_edge);
        sol.ub_c2 = sol.costs.second;
      }
    } else {
      cout << "no solution" << endl;
    }

    if(!res) {
      stop = true;
    }

    

  }

  // add the last solutions obtained previously
  solutions.push_back(sol_c2);

  // add the last lower bound edge
  bicriteria::Edge new_edge;
  new_edge.first = make_pair(sol_c2.costs.first, sol.ub_c2);
  new_edge.second = sol_c2.costs;
  lb_edges.push_back(new_edge);

  // filter eventual weakly dominated solutions
  vector<unsigned int> sol_indexes(solutions.size(), 0);
  for(unsigned int ind = 0; ind < solutions.size(); ind ++) {
    sol_indexes[ind] = ind;
  }

  for(auto it = sol_indexes.begin(); it != sol_indexes.end(); it ++) {

    bool dominated = false;
    if(it != sol_indexes.end()-1 && bicriteria::dominates(solutions[*(it+1)].costs, solutions[*it].costs) > 0) {
      dominated = true;
    }
    if(it != sol_indexes.begin() && bicriteria::dominates(solutions[*(it-1)].costs, solutions[*it].costs) > 0) {
      dominated = true;
    }

    if(dominated) {
      it = sol_indexes.erase(it);
    }

  }

  auto solutions_temp = solutions;
  solutions.clear();
  for(auto index: sol_indexes) {
    solutions.push_back(solutions_temp[index]);
  }

  auto t_end_total = std::chrono::high_resolution_clock::now();
  auto elapsed_time_total = std::chrono::duration<double, std::milli>(t_end_total-t_start_total).count();

  cout << "elapsed time: " << elapsed_time_total << " ms" << endl;

  // output solutions and costs

  if(_param.costs_fname != "") {
    ofstream fcosts(_param.costs_fname.c_str());
    if(fcosts) {
      for(auto& sol: solutions) {
        fcosts << sol.costs.first << " " << sol.costs.second << endl;  
      }
      fcosts.close();
    }
  }

  if(_param.sol_fname != "") {
    ofstream fsol(_param.sol_fname.c_str());
    if(fsol) {
      for(auto& sol: solutions) {
        for(auto& p: sol.values) {
          fsol << p.first << " " << p.second << " ";  
        }
        fsol << endl;  
      }
      fsol.close();
    }
  }

   if(_param.lb_fname != "") {
    ofstream flb(_param.lb_fname.c_str());
    if(flb) {
      flb << ideal_costs.first << " " << ideal_costs.second << endl;
      for(auto& edge: lb_edges) {
        flb << edge.first.first << " " << edge.first.second << " ";
        flb << edge.second.first << " " << edge.second.second << endl;
      }
      flb.close();
    }
  }

}

//------------------------------------------------------------------
bool EpsilonMethod::solve_mono(MultiCFN& mcfn12, bicriteria::SolutionEpsilon& solution) {

  bool solution_found = false;

  // return true if a solution has been found

  tb2init();

  if(Parameters::verbose > 0) {
      ToulBar2::verbose = 0;
  } else {
      ToulBar2::verbose = -1;
  }

  if(_param.vac) {
    ToulBar2::vac = 1;
  } else {
    ToulBar2::vac = 0;
  }

  ToulBar2::bisupport = _param.bisupport;

  // solver will contain the main wcsp
  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(MAX_COST);
  WCSP* wcsp = dynamic_cast<WCSP*>(solver->getWCSP());
    
  mcfn12.makeWeightedCSP(wcsp);

  try {

  bool result = false;

  // init toulbar2 timer
  ToulBar2::startCpuTime = cpuTime();
  timer(_param.timeout_phase2);

  result = solver->solve();

  // if no solution has been found, limited indicates if it is a proof
  solution.optimal = !ToulBar2::limited;
  solution.lb = solver->getDDualBound();

  nb_backtracks += solver->getNbBacktracks(); 

  // stop the timer
  timerStop();
  
  if(result) {

      solution_found = true;

      // correct optimality flag id the search returned known optimum
      if(!solution.optimal && ToulBar2::vnsOptimum == solver->getSolutionCost()) {
          solution.optimal = true;
      }

      solution.values = mcfn12.getSolution();
      auto costs = mcfn12.getSolutionValues();
      solution.costs = make_pair(costs[0], costs[1]); // the two cfn are always in mcfn1
      // }

      solution.ub = solver->getSolutionValue();
      if(solution.optimal) { // this should be fixed
          solution.lb = solution.ub;
      }


  }


  delete solver;

  return solution_found;

  } catch (const Contradiction&) {
    solution.optimal = true;
    delete solver;
    return false; 
  }

}


//------------------------------------------------------------------
bool EpsilonMethod::solve_bounds(MultiCFN& mcfn12, MultiCFN& mcfn2, std::vector<int>& global_scope, bicriteria::SolutionEpsilon& solution) {

  bool solution_found = false;

  // return true if a solution has been found

  tb2init();

  if(Parameters::verbose > 0) {
      ToulBar2::verbose = 0;
  } else {
      ToulBar2::verbose = -1;
  }

  ToulBar2::vac = 0;
  ToulBar2::bisupport = _param.bisupport;

  // solver will contain the main wcsp
  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(MAX_COST);
  WCSP* wcsp_main = dynamic_cast<WCSP*>(solver->getWCSP());
  mcfn12.makeWeightedCSP(wcsp_main);

  Cost c1_ub_cost = wcsp_main->DoubletoCost(solution.ub_c1);
 
  // cout << "c1 ub as cost: c1 < " << c1_ub_cost << endl;  
  wcsp_main->setUb(c1_ub_cost);
  // cout << "c1 negcost: " << wcsp_main->getNegativeLb() << endl;
  

  WCSP* wcsp_cst = dynamic_cast<WCSP*>(WeightedCSP::makeWeightedCSP(MAX_COST));

  mcfn2.setWeight(0, 1.);
  mcfn2.makeWeightedCSP(wcsp_cst);


  // UB for -c2 as cost (integers)
  Cost ub_c2_int = wcsp_cst->DoubletoCost(solution.ub_c2);

  if(Parameters::verbose >= 2) {
      cout << " c1 < " << c1_ub_cost << endl;
      cout << " c2 < " << ub_c2_int << endl;

      cout << "wcsp2 negcost: " << wcsp_cst->getNegativeLb() << endl;
      cout << "wcsp2 lb: " << wcsp_cst->getLb() << endl;
  }
  

  try {

  wcsp_main->postWeightedCSPConstraint(global_scope, wcsp_cst, nullptr, 0, ub_c2_int, _param.knapsack);
  
  bool result = false;

  // init toulbar2 timer
  ToulBar2::startCpuTime = cpuTime();
  timer(_param.timeout_phase2);

  result = solver->solve();

  // if no solution has been found, limited indicates if it is a proof
  solution.optimal = !ToulBar2::limited;
  solution.lb = solver->getDDualBound();

  nb_backtracks += solver->getNbBacktracks(); 

  // stop the timer
  timerStop();
  
  if(result) {

      solution_found = true;

      // correct optimality flag id the search returned known optimum
      if(!solution.optimal && ToulBar2::vnsOptimum == solver->getSolutionCost()) {
          solution.optimal = true;
      }

      solution.values = mcfn12.getSolution();
      auto costs = mcfn12.getSolutionValues();
      solution.costs = make_pair(costs[0], costs[1]); // the two cfn are always in mcfn1


      solution.ub = solver->getSolutionValue();
      if(solution.optimal) { // this should be fixed
          solution.lb = solution.ub;
      }


  }

  


  delete solver;
  delete wcsp_cst;

  return solution_found;

  } catch (const Contradiction&) {
    solution.optimal = true;
    solution.lb = wcsp_main->Cost2ADCost(c1_ub_cost);
    delete solver;
    delete wcsp_cst;
    return false; 
  }

}
