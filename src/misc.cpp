#include "misc.hpp"
#include "core/tb2wcsp.hpp"
#include "mcriteria/multicfn.hpp"



//---------------------------------------------------------------------------
void debugging() {

  tb2init();

  ToulBar2::cfn = true;
  // ToulBar2::xz = true;


  string fname1 = "instances/tests/3_15_11_1.cfn";
  string fname2 = "instances/tests/3_15_11_2.cfn";

  WCSP* wcsp1 = dynamic_cast<WCSP*>(WeightedCSP::makeWeightedCSP(MAX_COST));
  wcsp1->read_wcsp(fname1.c_str());

  cout << "initial lb: " << wcsp1->getLb() << endl;

  WCSP* wcsp2 = dynamic_cast<WCSP*>(WeightedCSP::makeWeightedCSP(MAX_COST));
  wcsp2->read_wcsp(fname2.c_str());

  MultiCFN multicfn;
  multicfn.push_back(wcsp1, 1.);
  // multicfn.push_back(wcsp2, 0.);

  MultiCFN::Solution sol;
  sol["w0"]="v1";
  sol["w1"]="v0";
  sol["w2"]="v0";
  for(int i = 0; i < 15; i++) {
    sol["s"+to_string(i)] = "v0";
  } 

  

  // multicfn.print(cout);

  tb2init();
  ToulBar2::verbose = 0;
  ToulBar2::showSolutions = 3;


  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(MAX_COST); 
  WCSP* wcsp_res = dynamic_cast<WCSP*>(solver->getWCSP());
  multicfn.makeWeightedCSP(wcsp_res);

  ofstream file("cfn_test.cfn");
  wcsp_res->dump_CFN(file);
  file.close();

  Double sol_cost = multicfn.computeSolutionValues(sol)[0]; 
  cout << "test solution mcfn: " << sol_cost << endl;

  cout << "sol cost - lb = " << sol_cost-wcsp_res->getLb() << endl;

  cout << "final ub:" << wcsp_res->Cost2ADCost(wcsp_res->getUb()) << endl;
  cout << "final lb:" << wcsp_res->Cost2ADCost(wcsp_res->getLb()) << endl;
  // wcsp_res->setUb(1783);


  bool result = solver->solve();

  if(result) {
    cout << multicfn.getSolutionValues() << endl;
    cout << "solve returns " << solver->getSolutionCost() << endl;
    cout << "solve returns (Double)" << solver->getSolutionValue() << endl;

  }

  delete solver;  
  delete wcsp1;
  delete wcsp2;

}
