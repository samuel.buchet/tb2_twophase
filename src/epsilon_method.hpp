#pragma once

#include "toulbar2lib.hpp"

#include "parameters.hpp"
#include "solution.hpp"

//------------------------------------------------------------------------
class EpsilonMethod {

  public:

    /* constructor */
    EpsilonMethod(Parameters& param);

    /* solve the problem with the epsilon constraint method */
    void epsilon_method();

  private:

    /* solve the bi criteria problem */
    void solve(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2);

    /* solve the problem with tb2 with bound on the second criteria */
    // param mcfn1 the multicfn containing the first criterion
    // param mcfn2 the multicfn containing the second criterion
    // param global_scope variable indexes between the first and the second cfn
    // solution the output solution, containing the bounds on the second criteria as input
    // return true iif a solution has been found
    bool solve_bounds(MultiCFN& mcfn1, MultiCFN& mcfn2, std::vector<int>& global_scope, bicriteria::SolutionEpsilon& solution);

    /* solve the problem by considering only one criterion */
    bool solve_mono(MultiCFN& mcfn, bicriteria::SolutionEpsilon& solution);

  private:

    Parameters& _param;

    int nb_backtracks;



};