
#include "utils.hpp"

#include "core/tb2wcsp.hpp"

#include <iostream>

using namespace std;

//---------------------------------------------------------------------------
unsigned int random_int(unsigned int min, unsigned int max) {
  return min + rand()%(max-min+1);
}

//---------------------------------------------------------------------------
double random_int(double min, double max) {
  unsigned int range = (max-min+1)*pow(10, 6);
  double res = min + (rand()%range)/pow(10, 6);
  return res;
}

//---------------------------------------------------------------------------
void init_global_scope(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2, vector<int>& scope) {

    WCSP* wcsp_1_ptr = dynamic_cast<WCSP*>(wcsp_1);
    WCSP* wcsp_2_ptr = dynamic_cast<WCSP*>(wcsp_2);

    scope.resize(wcsp_2_ptr->numberOfVariables());

    // build the scope of the global variable:
    // matching based on the variable names

    for(unsigned int var_ind = 0; var_ind < wcsp_2_ptr->numberOfVariables(); var_ind ++) {
        string name = wcsp_2_ptr->getVar(var_ind)->getName();
        scope[var_ind] = wcsp_1_ptr->getVarIndex(name);
    }

}

//---------------------------------------------------------------------------
bool set_tb2_extflags(const char* fileName) {

  bool test = false;

  if (strstr(fileName, ".xz") == &fileName[strlen(fileName) - strlen(".xz")]) {
    ToulBar2::xz = true;
    test = true;
  }

  if (strstr(fileName, ".gz") == &fileName[strlen(fileName) - strlen(".gz")]) {
    ToulBar2::gz = true;
    test = true;
  }

  if (strstr(fileName, ".bz2") == &fileName[strlen(fileName) - strlen(".bz2")]) {
    ToulBar2::bz2 = true;
    test = true;
  }

  if (strstr(fileName, ".cfn")) {
    ToulBar2::cfn = true;
    test = true;
  }

  if (strstr(fileName, ".wcnf") || strstr(fileName, ".cnf")) {
    ToulBar2::wcnf = true;
    test = true;
  }

  if (strstr(fileName, ".qpbo")) {
    ToulBar2::qpbo = true;
    test = true;
  }

  if (strstr(fileName, ".opb") || strstr(fileName, ".wbo")) {
    ToulBar2::opb = true;
    test = true;
  }

  if (strstr(fileName, ".lp")) {
    ToulBar2::lp = true;
    test = true;
  }

  if (strstr(fileName, ".uai")) {
    ToulBar2::uai = 1;
    ToulBar2::bayesian = true;
    test = true;
  }

  if (strstr(fileName, ".LG")) {
    ToulBar2::uai = 2;
    ToulBar2::bayesian = true;
    test = true;
  }

  //#if defined(XMLFLAG) || defined(XMLFLAG3)
  if (strstr(fileName, ".xml")) {
    ToulBar2::xmlflag = true;
    test = true;
  }
  //#endif

  if (strstr(fileName, ".wcsp")) {
    test = true;
  }

  tb2checkOptions();

  return test;
}

