#include "two_phases.hpp"

#include "toulbar2lib.hpp"
#include "core/tb2wcsp.hpp"

#include "lower_bound.hpp"
#include "solution.hpp"

#include "geometry.hpp"

using namespace std;

// global interruption of the search
extern bool global_interruption;

//----------------------------------------------------------------------------
bool bicriteria::TwoPhases::solve_scalarization(MultiCFN& mcfn, SolutionPhase1& solution) {

  // cout << "current weights: " << solution.weights << endl;

  // mcfn.setWeight(0, solution.weights.first);
  // mcfn.setWeight(1, solution.weights.second);

  // mcfn.print(cout);
  // cout << endl << endl;

  if(_solver == ToulBar2) {
    return solve_scalarization_HBFS(mcfn, solution);
  } else if(_solver == CPLEX) {
    // SolutionPhase1 solCPLEX(solution);
    
    // auto res1 = solve_scalarization_CPLEX(mcfn, solCPLEX);
    // cout << endl << endl;

    // auto res2 = solve_scalarization_HBFS(mcfn, solution);
    // cout << endl << endl;

    // cout << "Comparisons costs cplex vs tb2: " << endl;
    // cout << solCPLEX.ub << " vs " << solution.ub << endl;

    // cout << "Comparisons bicosts cplex vs tb2: " << endl;
    // cout << solCPLEX.costs << " vs " << solution.costs << endl;

    // // export toulbar2 solution
    // // ofstream file("sol_toulbar2.txt");
    // // for(auto& elt: solution.values) {
    // //   file << elt.first << " " << elt.second << endl;
    // // }
    // // file.close();

    // // cout << "Comparison solutions values: " << endl;
    // // cout << "cplex: ";
    // // for(auto& elt: solCPLEX.values) {
    // //   cout << elt.first << "=" << elt.second << ", ";
    // // }
    // // cout << endl << endl << "tb2: ";
    // // for(auto& elt: solution.values) {
    // //   cout << elt.first << "=" << elt.second << ", ";
    // // }
    // // cout << endl << endl;

    
    // // for(auto& elt: solCPLEX.values) {
    // //   if(solCPLEX.values[elt.first] != solution.values[elt.first]) {
    // //     cout << elt.first << "=" << elt.second  << " vs " << solution.values[elt.first] << ", ";
    // //   }
    // // }

    // cout << endl << endl;
    
    // cout << "comparison cplex sol ub and tb2 sol ub: " << solCPLEX.ub - solution.ub << endl; 
    // assert((!res1 && !res2) || fabs(solCPLEX.ub - solution.ub) <= 1e-4);

    // assert(res1 == res2);

    // return res2;

    #ifdef ILOGCPLEX
    return solve_scalarization_CPLEX(mcfn, solution);
    #else
    cout << "Error: the program has not been compiled with CPLEX integration, please change the makefile" << endl;
    return false;
    #endif
  } else {
    cerr << "error: solver undefined" << endl;
    exit(0);
  }

  return false;
}

//------------------------------------------------------------------------
bool bicriteria::TwoPhases::solve_scalarization_HBFS(MultiCFN& mcfn, bicriteria::SolutionPhase1& solution) {

  n_solve_phase1 ++;

  bool sol_found = false;

  mcfn.setWeight(0, solution.weights.first);
  mcfn.setWeight(1, solution.weights.second);

  if(Parameters::verbose >= 3) {
    cout << "weights: " << solution.weights << endl;
  }

  tb2init();

  if(_param.vac) {
    ToulBar2::vac = 1;
  } else {
    ToulBar2::vac = 0;
  }
  //ToulBar2::costThresholdPre = 100;
  //ToulBar2::useRASPS = 1;
  //ToulBar2::RASPSnbBacktracks = 1000;
  //ToulBar2::RASPSreset = true;
  //ToulBar2::preprocessTernaryRPC = 1;
  //ToulBar2::pwc = -1;
  //ToulBar2::hve = std::numeric_limits<tValue>::max();
  //ToulBar2::elimDegree_preprocessing = -8;
  //ToulBar2::elimSpaceMaxMB = 16;

  if(Parameters::verbose > 0) {
    ToulBar2::verbose = 0;
  } else {
    ToulBar2::verbose = -1;
  }

  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(MAX_COST);

  WCSP* wcsp = dynamic_cast<WCSP*>(solver->getWCSP());
  mcfn.makeWeightedCSP(wcsp);;

  if(solution.ub_c1 != std::numeric_limits<Double>::infinity()) {
    wcsp->setUb(wcsp->DoubletoCost(solution.ub_c1));
  }

  // solver->getWCSP()->dump_CFN(cout);

  // start the timer
  ToulBar2::startCpuTime = cpuTime();
  timer(_param.timeout_phase1);

  bool res = false;

  try {
    res = solver->solve();
  } catch(const TimeOut& ex) {
    if(Parameters::verbose >= 3) {
      cout << "timeout" << endl;
    }
  }

  // stop the timer
  timerStop();

  nb_nodes_phase1 += solver->getNbNodes();
  nb_backtracks_phase1 += solver->getNbBacktracks(); 

  solution.optimal = !ToulBar2::limited;
  solution.lb = solver->getDDualBound();

  if(res) {

    sol_found = true;

    solution.ub = solver->getSolutionValue();
    if(solution.optimal) { // this should be fixed
      solution.lb = solution.ub;
    }

    // get the bicriteria costs
    auto mcfn_solcosts = mcfn.getSolutionValues();
    solution.costs = make_pair(mcfn_solcosts[0], mcfn_solcosts[1]);

    // get the variable values
    solution.values = mcfn.getSolution();

  } else { // no solution found

    // cout << "error when solving the scalarization! " << endl; 
  }

  delete solver;

  return sol_found;
}


//------------------------------------------------------------------------
// remove the weakly dominated solutions from the set of supported solutions(indexes only)
void bicriteria::TwoPhases::remove_weaklydom(vector<std::shared_ptr<bicriteria::Solution>>& sol, vector<unsigned int>& indexes) {

  unsigned int start_index = 0;
  bool stop = false;
  while(!stop) {
    if(start_index >= sol.size()-1) {
      stop = true;
      continue;
    }
    if(bicriteria::equal(sol[indexes[start_index]]->costs.first, sol[indexes[start_index+1]]->costs.first)) {
      start_index ++;
    } else {
      stop = true;
    }
  }

  // cout << "start_index: " << start_index << endl; 

  unsigned int stop_index = indexes.size()-1;
  stop = false;
  while(!stop) {
    if(stop_index == 0) {
      stop = true;
      continue;
    }
    if(bicriteria::equal(sol[indexes[stop_index]]->costs.second, sol[indexes[stop_index-1]]->costs.second)) {
      stop_index --;
    } else {
      stop = true;
    }
  }

  // cout << "stop_index: " << stop_index << " vs " << indexes.size() << endl;

  unsigned int new_length = stop_index-start_index+1;
  if(start_index > 0) {
    for(unsigned int ind = 0; ind < new_length; ind ++) {
      indexes[ind] = indexes[ind+start_index];
    }
  }

  if(new_length < indexes.size()) {
    indexes.resize(new_length);
  }

}

//------------------------------------------------------------------------
Double dot(bicriteria::BiDouble v1, bicriteria::BiDouble v2) {
  return v1.first*v2.second - v1.second*v2.first;
}

//------------------------------------------------------------------------
bool bicriteria::TwoPhases::first_phase(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2, vector<std::shared_ptr<bicriteria::Solution>>& solutions, vector<pair<size_t, size_t>>& triangles, bicriteria::LowerBound& pareto_lb) {

  n_solve_phase1 = 0;

  nb_nodes_phase1 = 0;
  nb_backtracks_phase1 = 0;

  // exact is true if it has been possible to generate all the pareto optimal solutions in the convex hull of the pareto front
  bool exact = true;

  // TODO: when the hard constraints are duplicated between the two wcsp's, they are also duplicated in multiCFN (and not merged in toulbar2 as opposed to soft cost functions)
  ToulBar2::dumpWCSP = 1;
  MultiCFN mcfn;
  mcfn.push_back(dynamic_cast<WCSP*>(wcsp_1));
  mcfn.push_back(dynamic_cast<WCSP*>(wcsp_2));
  ToulBar2::dumpWCSP = 0;

  // pairs of solutions (indexes) from which to compute a weighted sum
  list<pair<unsigned int, unsigned int>> pending;

  // solve individually the two wcsp
  shared_ptr<SolutionPhase1> sol_c1 = make_unique<SolutionPhase1>();
  shared_ptr<SolutionPhase1> sol_c2 = make_unique<SolutionPhase1>();

  // solving c1
  sol_c1->weights = make_pair(1., 0.);
  sol_c1->ub_c1 = std::numeric_limits<Double>::infinity();
  bool result_c1 = solve_scalarization(mcfn, *sol_c1);
  if(Parameters::verbose >= 2) {
    if(result_c1) {
      cout << "optimum on c1: " << sol_c1->costs << endl;
    }
    cout << endl << endl;
  }

  if (!result_c1) return true; // problem has no solution, stop!
  else if (!sol_c1->optimal) {
    exact = false;
  }

  if(global_interruption) {
    if(Parameters::verbose >= 1) {
      cout << "Interrupting phase 0!" << endl;
    }  
    return false;
  }

  // solving c2
  sol_c2->weights = make_pair(0., 1.);
  sol_c2->ub_c1 = std::numeric_limits<Double>::infinity();
  bool result_c2 = solve_scalarization(mcfn, *sol_c2);
  if(Parameters::verbose >= 2) {
    if(result_c2) {
      cout << "optimum on c2: " << sol_c2->costs << endl;
    }
  }

  if (!result_c2) return true; // problem has no solution, stop!
  else if (!sol_c2->optimal) {
    exact = false;
  }

  // an ideal point is computed at the begining from the two first scalarizations
  // it should be stored since weakly dominated solution may be lost
  if(result_c1 && result_c2) { // if not true, the pb has no solution
    pareto_lb.ideal_costs = make_pair(sol_c1->lb, sol_c2->lb);
    pareto_lb.ideal_c1_optimal = sol_c1->optimal;
    pareto_lb.ideal_c2_optimal = sol_c2->optimal;
    nadir_costs = make_pair(sol_c2->costs.first, sol_c1->costs.second);
  }

  if(result_c1) {
    solutions.push_back(sol_c1);
  }

  // make sure solcosts_c1 != solcosts_c2, otherwise all points are equal or on the same line
  // normally result_c1 = result_c2
  if(result_c1 && sol_c1->optimal && result_c2 && sol_c2->optimal && !equal(sol_c1->costs, sol_c2->costs)) {
    solutions.push_back(sol_c2);
    pending.push_front(make_pair(0, 1));
  }

  // solve linear combinations of the two wcsp's

  while(!pending.empty()) {

    // stop the search if the total number of call to solve has been reach
    if(n_solve_phase1 >= _param.n_solve_limit) {
      exact = false;
      break;
    }

    // stop the search if there is a global interruption
    if(global_interruption) {
      exact = false;
      if(Parameters::verbose >= 1) {
        cout << "Interrupting phase 1!" << endl;
      }  
      break;
    }

    if(Parameters::verbose >= 1) {
      cout << endl << endl;
    }

    auto current = pending.front();
    pending.pop_front();

    // warning: using auto& here does not work
    shared_ptr<Solution> sol_left = solutions[current.first];
    shared_ptr<Solution> sol_right = solutions[current.second];

    shared_ptr<bicriteria::SolutionPhase1> sol = make_unique<bicriteria::SolutionPhase1>();

    // y2l - y2r, y1r - y1l
    sol->weights = make_pair(sol_left->costs.second-sol_right->costs.second, sol_right->costs.first-sol_left->costs.first);

    // make sure weights are integer so that no precision is lost
    if(mcfn.getDecimalPoint() > 0) {
      sol->weights.first *= pow(10, mcfn.getDecimalPoint());
      sol->weights.second *= pow(10, mcfn.getDecimalPoint());
    }

    if(Parameters::verbose >= 2) {
      cout << "Solving scalarization with weights: " << sol->weights << endl;
      cout << "coming from " << sol_left->costs << " and " << sol_right->costs << endl;
    }

    sol->ub_c1 = sol_left->costs.first*sol->weights.first+sol_left->costs.second*sol->weights.second;

    
    //assert(fabs(sol->ub_c1 - (sol_right->costs.first*sol->weights.first+sol_right->costs.second*sol->weights.second)) > bicriteria::epsilon);

    bool result = solve_scalarization(mcfn, *sol);

    // if there exist no solution, stop here and store the triangle
    if(!result) {
      if(sol->optimal && sol_left->optimal && sol_right->optimal) {
        if(Parameters::verbose >= 2) {
          cout << " early stop, no solution" << endl;
        }
        triangles.push_back(current);
        pareto_lb.lines_phase1.push_back(make_pair(sol->weights, sol->ub_c1));
      } else {
        exact = false;
      }

      continue;
    }

    // the solution should not be aligned with the previous one
    assert(fabs(sol->ub_c1 - sol->weights.first*sol->costs.first-sol->weights.second*sol->costs.second) >= bicriteria::epsilon);
 
    
    if(Parameters::verbose >= 2) {
      cout << "new point: " << sol->costs << endl;
    }

    if(!sol->optimal) {
      exact = false;
      //cout << endl << endl << " test " << endl << endl << endl;
    }

    if(Parameters::verbose >= 2) {
      cout << "Optimality ? " << sol->optimal << endl;
    }

    // if at least one solution exists, this should be always true
    // assert(result);

    if(!equal(sol->costs, sol_left->costs) && !equal(sol->costs, sol_right->costs)) {

      // add the new solution
      solutions.push_back(sol);

      // add the lb from the solution to the set of lower bounds
      pareto_lb.lines_phase1.push_back(make_pair(sol->weights, sol->lb));

      // the following should not happen with the initial ub

      // verify the new point is in-between the original ones
      assert(!sol_left->optimal || !sol_right->optimal || !sol->optimal || (sol->costs.first >= sol_left->costs.first && sol->costs.first <= sol_right->costs.first));
      assert(!sol_left->optimal || !sol_right->optimal || !sol->optimal || (sol->costs.second >= sol_right->costs.second && sol->costs.second <= sol_left->costs.second));

      // let us be lucky: if the new point is on the same line, the dichotomy stops here
      Double scal = sol_left->costs.first*sol->weights.first+sol_left->costs.second*sol->weights.second;

      if(sol->optimal && sol_left->optimal && sol_right->optimal && equal(scal, sol->ub)) {
        triangles.push_back(make_pair(current.first, solutions.size()-1));
        triangles.push_back(make_pair(solutions.size()-1, current.second));
      } else {
        
        // add the new scalarizations only if the direction is consistent with the dichotomic search
        
        

        // -> from left to right
        shared_ptr<SolutionPhase1> sol_left_ptr = dynamic_pointer_cast<SolutionPhase1>(sol_left);
        // sol->weights;
        shared_ptr<SolutionPhase1> sol_right_ptr = dynamic_pointer_cast<SolutionPhase1>(sol_right);

        BiDouble new_left = make_pair(sol_left_ptr->costs.second-sol->costs.second, sol->costs.first-sol_left_ptr->costs.first);
        BiDouble new_right = make_pair(sol->costs.second-sol_right_ptr->costs.second, sol_right_ptr->costs.first-sol->costs.first);

        if(Parameters::verbose >= 2) {
          cout << "future weights left: " << new_left << endl;
          cout << "future weights right: " << new_right << endl;
        }

        if(Parameters::verbose >= 3) {
          cout << "two dot products, new_left: " << dot(sol_left_ptr->weights, new_left) << " and " << dot(new_left, sol->weights) << endl;
          cout << "two dot products, new_right: " << dot(sol->weights, new_right) << " and " << dot(new_right, sol_right_ptr->weights) << endl;
        }

        if((sol->optimal && sol_left->optimal && sol_right->optimal) || (dot(sol->weights, new_right) >= 0 && dot(new_right, sol_right_ptr->weights) >= 0)) {
          if(new_right.first > bicriteria::epsilon && new_right.second > bicriteria::epsilon) {
            pending.push_front(make_pair(solutions.size()-1, current.second));
          }
        }

        if((sol->optimal && sol_left->optimal && sol_right->optimal) || (dot(sol_left_ptr->weights, new_left) >= 0 && dot(new_left, sol->weights) >= 0)) {
          if(new_left.first > bicriteria::epsilon && new_left.second > bicriteria::epsilon) {
            pending.push_front(make_pair(current.first, solutions.size()-1));
          }
        }

        // if( (sol->optimal && sol_left->optimal && sol_right->optimal) || (sol->costs.first >= sol_left->costs.first && sol->costs.first <= sol_right->costs.first && sol->costs.second >= sol_right->costs.second && sol->costs.second <= sol_left->costs.second )) {
        //   pending.push_front(make_pair(solutions.size()-1, current.second));
        //   pending.push_front(make_pair(current.first, solutions.size()-1));
        // }
      
      }
      
    } else if(sol->optimal) {
      // we obtained the proof that no solutions exist beyond two points -> new triangle
      triangles.push_back(current);
      pareto_lb.lines_phase1.push_back(make_pair(sol->weights, sol->lb)); // a new line for lb is known
    }

  }

  vector<unsigned int> sol_indexes(solutions.size(), 0);
  for(unsigned int ind = 0; ind < solutions.size(); ind ++) {
    sol_indexes[ind] = ind;
  }

  // remove dominated solutions
  if(!exact) {
    for(auto it = sol_indexes.begin(); it != sol_indexes.end();) {
      bool dom = false;
      for(auto it2 = sol_indexes.begin(); it2 != sol_indexes.end(); it2 ++) {
        if(it2 == it) {
          continue;
        }
        if(dominates(solutions[*it2]->costs, solutions[*it]->costs) > 0) {
          dom = true;
          break;
        }
      }
      if(dom) {
        it = sol_indexes.erase(it);
      } else {
        it ++;
      }
    }
    
  }

  // sort the solutions
  auto lambda_sort = [solutions](auto& left, auto& right) { return solutions[left]->costs.first < solutions[right]->costs.first || (equal(solutions[left]->costs.first, solutions[right]->costs.first) && solutions[left]->costs.second > solutions[right]->costs.second); };

  sort(sol_indexes.begin(), sol_indexes.end(), lambda_sort);

  // remove the weakly dominated solutions if they exist
  if(exact) {
    remove_weaklydom(solutions, sol_indexes);
  }

  // modify the indexes in the edge vector according to the new order
  map<unsigned int, unsigned int> sol_permutation;
  for(unsigned int ind = 0; ind < sol_indexes.size(); ind ++) {
    sol_permutation[sol_indexes[ind]] = ind;
  }

  for(auto it = triangles.begin(); it != triangles.end();) {
    // make sure the solutions of the edge are still part of the output
    if(sol_permutation.find(it->first) == sol_permutation.end() || sol_permutation.find(it->second) == sol_permutation.end()) {
      it = triangles.erase(it);
      continue;
    }
    it->first = sol_permutation[it->first];
    it->second = sol_permutation[it->second];
    it ++;
  }


  /* create the final list of solutions */
  auto sol_temp = solutions;
  solutions.clear();
  for(auto& index: sol_indexes) {
    solutions.push_back(sol_temp[index]);
  }

  // filter dominated solutions

  return exact;
}


