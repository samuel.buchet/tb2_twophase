#pragma once

#include "toulbar2lib.hpp"
#include "mcriteria/multicfn.hpp"

#include "parameters.hpp"

#include <memory>
#include <chrono>

namespace bicriteria {

// forward declarations
class Solution;
class SolutionPhase1;
class SolutionPhase2;
class LowerBound;

typedef std::pair<Double, Double> BiDouble; // double costs
typedef std::pair<BiDouble, BiDouble> Edge; // edge defined by two points
typedef std::pair<BiDouble, Double> Line; // Line defined by two coefficients and a scalar

const Double epsilon = 1e-7;

//------------------------------------------------------------------------
class TwoPhases {

  public:
    enum Solver { ToulBar2, CPLEX };

  /* options */
  public:

    static TwoPhases* _instance;

    Parameters _param;

    // output
    int n_solve_phase1; // number of call to solve in phase 1
    int n_solve_phase2; // number of call to solve in phase 2

    Double final_gap;
    int nbNodes;
    int nbBacktracks;
    bool timeout;

    unsigned long long nb_nodes_phase1;
    unsigned long long nb_backtracks_phase1;

    unsigned long long nb_nodes_phase2;
    unsigned long long nb_backtracks_phase2;

    // internal
    BiDouble nadir_costs; // initial ub obtain after the first solution

  private:
    
    Solver _solver;

    #ifdef ILOGCPLEX
    std::shared_ptr<IloCplex::Aborter> cplex_aborter; // cplex aborter
    #endif

  public:

    // constructor
    TwoPhases(Parameters& param): _param(param) {

      _instance = this;

      n_solve_phase1 = 0;
      n_solve_phase2 = 0;
      final_gap = 1e100;
      nbNodes = 0;
      nbBacktracks = 0;
      timeout = false;
      nb_nodes_phase1 = 0;
      nb_backtracks_phase1 = 0;
      nb_nodes_phase2 = 0;
      nb_backtracks_phase2 = 0;

      if(param.use_cplex) {
        _solver = CPLEX;
      } else {
        _solver = ToulBar2;
      }
    }

    /*!
     * \brief get the current solver instance
     */
    static TwoPhases* getInstance() {
      return _instance;
    }

    // assume cfn file names are given as parameters
    void two_phases();

    /*! 
      * \brief run the two phases method on two cfns
      * \param cfn_1 the name of the first cfn file
      * \param cfn_2 the name of the second cfn file
      */
    void two_phases(std::string cfn_1, std::string cfn_2);

    //------------------------------------------------------------------------
    void two_phases(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2);

    /*!
      * \brief compute or approximate the convex hull of the pareto front
      * \param wcsp_1 the first criterion
      * \param wcsp_2 the second criterion
      * \param solutions the solutions returned by the method
      * \param triangles a set of pairs of solution indexes that are optimal, forming triangles to explore in phase 2
      * \param pareto_lb the lower bound built from the first phase
      * \return true if the result is exact, false otherwise
    */
    bool first_phase(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2, std::vector<std::shared_ptr<Solution>>& solutions, std::vector<std::pair<size_t, size_t>>& triangles, LowerBound& pareto_lb);

    /*!
      * \brief complete a pareto front partially obtained on the first phase
      * \param wcsp_1 the first criterion
      * \param wcsp_2 the second criterion
      * \param solutions the solutions from the first phase
      * \param triangles tset of optimal solutions indexes from phase 1
      * \param pareto_lb the lower bound built from the search
      * \param combination if true, a linear combination of the two criteria will be optimized instead of only the first one, using the weights obtained from the triangle
      * \return true if the result is exact, false otherwise
      */
    bool second_phase(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2,  std::vector<std::shared_ptr<Solution>>& solutions, std::vector<std::pair<size_t, size_t>>& triangles_phase1, LowerBound& pareto_lb);



    //------------------------------------------------------------------------
    void solve_epsilon_constraint(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2);

    /*!
     * \brief abort the current search
     */
    void abort();

  private:

    /* first phase */

    /*!
    * \brief solve a scalarization
    * \param mcfn the multicfn containing the two criteria
    * \param solution a solution object containing the weights and returning the solution
    * \return true if a solution has been found, false otherwise
    */
    bool solve_scalarization(MultiCFN& mcfn, SolutionPhase1& solution);

    #ifdef ILOGCPLEX

    /*!
    * \brief solve a scalarization with CPLEX
    * \param mcfn the multicfn containing the two criteria
    * \param solution a solution object containing the weights and returning the solution
    * \return true if a solution has been found, false otherwise
    */
    bool solve_scalarization_CPLEX(MultiCFN& mcfn, SolutionPhase1& solution);

    #endif

    /*!
    * \brief solve a scalarization with toulbar2 solver (HBFS algorithm)
    * \param mcfn the multicfn containing the two criteria
    * \param solution a solution object containing the weights and returning the solution
    * \return true if a solution has been found, false otherwise
    */
    bool solve_scalarization_HBFS(MultiCFN& mcfn, SolutionPhase1& solution);

    //------------------------------------------------------------------------
    void remove_weaklydom(std::vector<std::shared_ptr<Solution>>& sol, std::vector<unsigned int>& indexes);

    /*!
      * \brief enumerate all the solutions corresponding to the bi-costs found fduring the two phases search
      * \param wcsp_1 the first criterion of the problem
      * \param wcsp_2 the second criterion of the problem
      * \param solutions the list of solutions found during the search
      * \param all_solutions_phase1 output of the solutions corresponding to bi-costs found during the first phase
      * \param all_solutions_phase2 output of the solutions corresponding to bi-costs found during the second phase
      */
    void enumerate_solutions(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2, vector<std::shared_ptr<bicriteria::Solution>>& solutions, vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase1, vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase2);


    /* second phase */


    /*!
      * \brief enumerate all the solutions corresponding to the bi-cost found in the pareto frontiere in phase 1
      * \param mcfn the multicfn containing the two criteria
      * \param solutions the solutions found in phase 1
      * \param all_solutions_phase1
      */
    void enum_sol_phase1(MultiCFN& mcfn, std::shared_ptr<bicriteria::Solution>& solutions, std::vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase1);

    /*!
    * \brief solve one iteration of the epsilon method given bounds
    * \param mcfn1 the multicfn containing the main wcsp (first criterion)
    * \param mcfn2 the multicfn containing the constraint wcsp (second criterion)
    * \param mcfn_total a multicfn containing the two criteria
    * \param global_scope a matching between the variables of the two criteria
    * \param solution the solution of the problem, contains the lower and upper bounds of the epsilon constraint method
    * \return true if a solution (optimal or not) has been found
    */
    bool solve_epsilon(MultiCFN& mcfn1, MultiCFN& mcfn2, MultiCFN& mcfn_total, std::vector<int>& global_scope, SolutionPhase2& solution);

    #ifdef ILOGCPLEX

    /*!
     * \brief solve the epsilon method with the CPLEX solver
     */
    bool solve_epsilon_CPLEX(MultiCFN& mcfn_total, bicriteria::SolutionPhase2& solution);

    #endif

    /*!
     * \brief solve the epsilon method with HBFS algorithm of the toulbar2 solver
     */
    bool solve_epsilon_HBFS(MultiCFN& mcfn1, MultiCFN& mcfn2, std::vector<int>& global_scope, bicriteria::SolutionPhase2& solution);


    Double computeUBArea(BiDouble bounds_c1, BiDouble bounds_c2, std::vector<std::shared_ptr<bicriteria::Solution>>& solutions);

    /*!
      * \brief enumerate all the solutions corresponding to the bi-cost found in the pareto frontiere in phase 1
      * \param mcfn1 the multicfn containing the first criterion
      * \param mcfn2 the multicfn containing the second criterion
      * \param solutions the solutions found in the two phases
      * \param all_solutions_phase2
      */
    void enum_sol_phase2(MultiCFN& mcfn1, MultiCFN& mcfn2, std::vector<int>& global_scope,  std::shared_ptr<bicriteria::Solution>& solutions, std::vector<std::shared_ptr<bicriteria::Solution>>& all_solutions_phase2);

    /*!
      * \brief helper function to clean a wcsp when a contradiction occurs beofre solving
      * \param wcsp the wcsp ibject to clean
      */
    static void clean_wcsp(WCSP* wcsp);

};











} // namespace twophase
