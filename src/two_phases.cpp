#include "core/tb2wcsp.hpp"

#include "core/tb2globalwcsp.hpp"

#include "two_phases.hpp"
#include "lower_bound.hpp"
#include "solution.hpp"
#include "utils.hpp"
#include "geometry.hpp"

#include <unistd.h>
#include <sys/times.h>
#include <sys/stat.h>

#include <thread>
#include <mutex>

using namespace std;


extern bool global_interruption;
extern std::mutex interruption_mutex;

bicriteria::TwoPhases* bicriteria::TwoPhases::_instance = nullptr;

//-------------------------------------------------------------------------
void global_timer(int timeout) {

  std::this_thread::sleep_for(std::chrono::seconds(timeout));

  std::lock_guard<std::mutex> guard(interruption_mutex);
  global_interruption = true;

  /* toulbar2 timeout function */
  bicriteria::TwoPhases* solver = bicriteria::TwoPhases::getInstance();
  if(solver != nullptr) {
    solver->abort();
  }

}

//-------------------------------------------------------------------------
void bicriteria::TwoPhases::abort() {

  if(_solver == ToulBar2) { /* toulbar2 timeout function */
    
    timeOut(0);

  } else { /* cplex timeout */
    #ifdef ILOGCPLEX
    if(cplex_aborter.get() != nullptr && !cplex_aborter.get()->isAborted()) {
      cplex_aborter.get()->abort();
      // cplex_aborter.get()->clear();
      cout << "cplex abort" << endl;
    }
    #endif
  }
  

}

//-------------------------------------------------------------------------
void bicriteria::TwoPhases::two_phases() {
  two_phases(_param.cfn_1, _param.cfn_2);
}

//-------------------------------------------------------------------------
void bicriteria::TwoPhases::two_phases(string cfn_1, string cfn_2) {

  tb2init();
  initCosts();

  if(Parameters::verbose > 0) {
    ToulBar2::verbose = 0;
  } else {
    ToulBar2::verbose = -1;
  }

  // check for existance of the files
  struct stat buffer;
  if(stat (cfn_1.c_str(), &buffer) != 0) {
    cerr << "Error, file " << cfn_1 << " does not exist!" << endl;
    return;
  }
  if(stat (cfn_2.c_str(), &buffer) != 0) {
    cerr << "Error, file " << cfn_2 << " does not exist!" << endl;
    return;
  }

  set_tb2_extflags(cfn_1.c_str());

  WeightedCSP* wcsp1 = nullptr;
  WeightedCSP* wcsp2 = nullptr;

  // do not propagate when reading the two problems in order to keep them as they are before converting into MultiCFN 
  ToulBar2::dumpWCSP = 1;

  /* first criterion */
  wcsp1 = WeightedCSP::makeWeightedCSP(MAX_COST);
  dynamic_cast<WCSP*>(wcsp1)->read_wcsp(cfn_1.c_str());
  wcsp1->sortConstraints();

  /* second criterion */
  wcsp2 = WeightedCSP::makeWeightedCSP(MAX_COST);
  dynamic_cast<WCSP*>(wcsp2)->read_wcsp(cfn_2.c_str());
  wcsp2->sortConstraints();

  ToulBar2::dumpWCSP = 0;

  // start the global timer
  std::thread timer_thread(global_timer, _param.global_timeout);
  timer_thread.detach();

  // run the two phase method
  two_phases(wcsp1, wcsp2);

  /* dummy solve call to make sure memory is cleared */
  /*ToulBar2::verbose = -1;
  wcsp1->setLb(wcsp1->getUb()-MIN_COST);
  WeightedCSPSolver* solver = WeightedCSPSolver::makeWeightedCSPSolver(wcsp1->getUb(), wcsp1);
  solver->solve();
  delete solver;

  wcsp2->setLb(wcsp2->getUb()-MIN_COST);
  solver = WeightedCSPSolver::makeWeightedCSPSolver(wcsp2->getUb(), wcsp2);
  solver->solve();
  delete solver;*/


  delete wcsp1;
  delete wcsp2;
}



//-------------------------------------------------------------------------
void bicriteria::TwoPhases::two_phases(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2) {
  
  // required to compute User time
  clock_t tics_per_second = sysconf(_SC_CLK_TCK);

  struct tms t_begin, t_before, t_after;

  double elapsed_time_enumeration = 0.;

  vector<std::shared_ptr<bicriteria::Solution>> solutions;
  bicriteria::LowerBound pareto_lb(solutions); /* lower bound set on the pareto set */
  vector<pair<size_t, size_t>> triangles_phase1;

  /* optional enumeration of all solutions */
  std::vector<std::shared_ptr<bicriteria::Solution>> all_solutions_phase1, all_solutions_phase2;

  if(Parameters::verbose >= 1) {
    cout << endl;
    cout << "***************" << endl;
    cout << "Running phase 1" << endl; 
    cout << "***************" << endl;
    cout << endl;
  }

  if(times(&t_begin) < 0) {
    cerr << "error while accessing execution time" << endl;
  }

  bool complete_phase1 = first_phase(wcsp_1, wcsp_2, solutions, triangles_phase1, pareto_lb);

  unsigned int n_solutions_phase1 = solutions.size();
  
  if(times(&t_after) < 0) {
    cerr << "error while accessing execution time" << endl;
  }
 
  double elapsed_time_phase1 = (double)(t_after.tms_utime-t_begin.tms_utime)/tics_per_second;

  // BiDouble init_bounds_c1 = make_pair(wcsp_1->Cost2ADCost(wcsp_1->getLb()), wcsp_1->Cost2ADCost(wcsp_1->finiteUb()-UNIT_COST));
  // BiDouble init_bounds_c2 = make_pair(wcsp_2->Cost2ADCost(wcsp_2->getLb()), wcsp_2->Cost2ADCost(wcsp_2->finiteUb()-UNIT_COST));

  BiDouble init_bounds_c1 = make_pair(pareto_lb.ideal_costs.first, nadir_costs.first);
  BiDouble init_bounds_c2 = make_pair(pareto_lb.ideal_costs.second, nadir_costs.second);
  

  if(Parameters::verbose >= 1) {
    cout << init_bounds_c1.first << " <= c1 <= " << init_bounds_c1.second << endl;
    cout << init_bounds_c2.first << " <= c2 <= " << init_bounds_c2.second << endl;
  }

  Double initial_gap = (init_bounds_c1.second-init_bounds_c1.first) * (init_bounds_c2.second - init_bounds_c2.first);

  // {
  // ofstream file;
  // file.open("pareto_costs_phase1.txt");
  // for(auto& sol: solutions) {
  //   file << sol->costs.first << " " << sol->costs.second << endl;
  // }
  // file.close();
  // }

  if(Parameters::verbose >= 1) {
    cout << endl;
    cout << "***************" << endl;
    cout << "Running phase 2" << endl; 
    cout << "***************" << endl;
    cout << endl;
  }

  if(times(&t_before) < 0) {
    cerr << "error while accessing execution time" << endl;
  }
 
  // initialize the lower bound for phase 2 (necessary also when phase 2 is not executed)
  for(auto& edge: triangles_phase1) {
    pareto_lb.edges_phase2.push_back(vector<Edge>());
    // a first lb of the triangle is the lb from phase 1
    pareto_lb.edges_phase2.back().push_back(make_pair(solutions[edge.first]->costs, solutions[edge.second]->costs));
  }

  bool complete_phase2 = false;

  if(triangles_phase1.size() > 0 && !global_interruption) {
    complete_phase2 = second_phase(wcsp_1, wcsp_2, solutions, triangles_phase1, pareto_lb);
  } else if(complete_phase1) { // the phase one may be incomplete and return no triangles
    complete_phase2 = true;
  }

  if(times(&t_after) < 0) {
    cerr << "error while accessing execution time" << endl;
  }



  // sort the solutions
  vector<unsigned int> sol_indexes(solutions.size(), 0);
  for(unsigned int ind = 0; ind < solutions.size(); ind ++) {
    sol_indexes[ind] = ind;
  }
  auto lambda_sort = [solutions](auto& left, auto& right) { return solutions[left]->costs.first < solutions[right]->costs.first || (equal(solutions[left]->costs.first, solutions[right]->costs.first) && solutions[left]->costs.second < solutions[right]->costs.second); };
  sort(sol_indexes.begin(), sol_indexes.end(), lambda_sort);
  

  // remove dominated solutions
  for(auto it = sol_indexes.begin(); it != sol_indexes.end();) {
    bool dom = false;
    for(auto it2 = sol_indexes.begin(); it2 != sol_indexes.end(); it2 ++) {
      if(it2 == it) {
        continue;
      }
      if(dominates(solutions[*it2]->costs, solutions[*it]->costs) > 0) {
        dom = true;
        break;
      }
    }
    if(dom) {
      it = sol_indexes.erase(it);
    } else {
      it ++;
    }
  }

  auto sol_temp = solutions;
  solutions.clear();
  for(auto& index: sol_indexes) {
    solutions.push_back(sol_temp[index]);
  }

  // modify the indexes in the edge vector according to the new order
  map<unsigned int, unsigned int> sol_permutation;
  for(unsigned int ind = 0; ind < sol_indexes.size(); ind ++) {
    sol_permutation[sol_indexes[ind]] = ind;
  }
  for(auto it = triangles_phase1.begin(); it != triangles_phase1.end();) {
    // make sure the solutions of the edge are still part of the output
    if(sol_permutation.find(it->first) == sol_permutation.end() || sol_permutation.find(it->second) == sol_permutation.end()) {
      it = triangles_phase1.erase(it);
      continue;
    }
    it->first = sol_permutation[it->first];
    it->second = sol_permutation[it->second];
    it ++;
  }




  double elapsed_time_phase2 = (double)(t_after.tms_utime-t_before.tms_utime)/tics_per_second;


  unsigned int n_solutions_phase2 = solutions.size()-n_solutions_phase1;


  // do not enumerate all solutions if the search is not complete
  if(!complete_phase1 || !complete_phase2 || global_interruption) {
    _param.all_solutions = false;
  }

  // enumeration of all the solutions (optional)
  if(_param.all_solutions) {

    if(Parameters::verbose >= 1) {
      cout << endl;
      cout << "****************************" << endl;
      cout << "Running solution enumeration" << endl; 
      cout << "****************************" << endl;
      cout << endl;
    }

    if(Parameters::verbose >= 1) {
      cout << endl << endl;
      cout << "Enumeration of all solutions in Phase 1" << endl;
    }

    if(times(&t_before) < 0) {
      cerr << "error while accessing execution time" << endl;
    }

    enumerate_solutions(wcsp_1, wcsp_2, solutions, all_solutions_phase1, all_solutions_phase2);

    if(times(&t_after) < 0) {
      cerr << "error while accessing execution time" << endl;
    }

    elapsed_time_enumeration = (double)(t_after.tms_utime-t_before.tms_utime)/tics_per_second;
  }


  
  // compute the lowerbound on the pareto frontiere

  if(Parameters::verbose >= 1) {
    cout << "compute lower bound" << endl;
  }

  pareto_lb.computeLowerBound(solutions, triangles_phase1);

  if(times(&t_after) < 0) {
    cerr << "error while accessing execution time" << endl;
  }

  double elapsed_time_total = (double)(t_after.tms_utime-t_begin.tms_utime)/tics_per_second;

  Double UB_area = (solutions.size() > 0) ? computeUBArea(init_bounds_c1, init_bounds_c2, solutions) : 0.;
  // cout << "debug: ub area: " << UB_area << endl;
  // cout << "debug sol size: " << solutions.size() << endl;
  Double LB_area = ( solutions.size() >= 2 && !equal(pareto_lb.ideal_costs, nadir_costs) ? pareto_lb.computeLBArea(init_bounds_c1, init_bounds_c2) : UB_area );
  
  Double optimality_gap = (fabs(initial_gap) >= bicriteria::epsilon) ? (LB_area-UB_area)/initial_gap : 0.;
  
  optimality_gap *= 100.;
  
  // if(LB_area <= bicriteria::epsilon && UB_area <= bicriteria::epsilon) {
  //   optimality_gap = 100.;
  // }
  if (!complete_phase1 && solutions.size()<=1) {
    optimality_gap = 100.;
  }

  cout << std::fixed << std::setprecision(6);

  cout << "first criterion: " << _param.cfn_1 << endl;
  cout << "second criterion: " << _param.cfn_2 << endl;

  cout << "Ideal point: " << pareto_lb.ideal_costs << endl;
  cout << "Nadir point: " << nadir_costs << endl;

  cout << "UB area : " << UB_area << endl;

  cout << "LB area : " << LB_area << endl;
  
  cout << "initial gap: " << initial_gap;
  cout << " -> (" <<  init_bounds_c1.second << " - " << init_bounds_c1.first << ")";
  cout << " * (" << init_bounds_c2.second << " - " << init_bounds_c2.first << ")" << endl;

  // (init_bounds_c1.second-init_bounds_c1.first) * (init_bounds_c2.second - init_bounds_c2.first)

  cout << "final gap: " << (optimality_gap >= 100.-bicriteria::epsilon ? initial_gap: (LB_area-UB_area)) << endl;

  cout << "final optimality gap (pct): " << optimality_gap << "%" << endl;

  cout << "n call to solve phase 1: " << n_solve_phase1 << endl;
  cout << "n call to solve phase 2: " << n_solve_phase2 << endl;

  cout << std::fixed << std::setprecision(2);

  cout << "nb nodes phase 1: " << nb_nodes_phase1 << " (" << (float)nb_nodes_phase1/n_solve_phase1 << " per solve)" << endl;
  cout << "nb backtracks phase 1: " << nb_backtracks_phase1 << " (" << (float)nb_backtracks_phase1/n_solve_phase1 << " per solve)" << endl;

  if(nb_nodes_phase2 > 0 && nb_backtracks_phase2 > 0) {
    cout << "nb nodes phase 2: " << nb_nodes_phase2 << " (" << (float)nb_nodes_phase2/n_solve_phase2 << " per solve)" << endl;
    cout << "nb backtracks phase 2: " << nb_backtracks_phase2 << " (" << (float)nb_backtracks_phase2/n_solve_phase2 << " per solve)" << endl;
  } else {
    cout << "nb nodes phase 2: " << nb_nodes_phase2 << endl;
    cout << "nb backtracks phase 2: " << nb_backtracks_phase2 << endl;
  }

  cout << std::fixed << std::setprecision(6);

  // cout << "nb nodes: " << nb_nodes_phase1+nb_nodes_phase2 << endl;
  // cout << "nb backtracks: " << nb_backtracks_phase1+nb_backtracks_phase2 << endl;

  cout << "n solutions phase 1: " << n_solutions_phase1 << endl;
  cout << "n solutions phase 2: " << n_solutions_phase2 << endl;

  cout << "n solutions at the end of solving: " << solutions.size() << endl;

  if(_param.all_solutions) {
    cout << "n solutions after enumeration: " << all_solutions_phase1.size() + all_solutions_phase2.size() << endl;
  }

  cout << "timeout phase 1 " << !complete_phase1 << endl;
  cout << "timeout phase 2 " << !complete_phase2 << endl; 
  
  cout << "Elapsed time in phase 1: " << elapsed_time_phase1 << " s" << endl;

  cout << "Elapsed time in phase 2: " << elapsed_time_phase2 << " s" << endl;

  if(_param.all_solutions) {
    cout << "Elapsed time in solution enumeration: " << elapsed_time_enumeration << " s" << endl;
  }

  cout << "Elapsed time in total: " << elapsed_time_total << " s" << endl;

  {
  ofstream file;
  file.open("lines_phase1.txt");
  for(auto& elt: pareto_lb.lines_phase1) {
    file << elt.first.first << " " << elt.first.second << " " << elt.second << endl;
  }
  file.close();
  }
  


  // outputing the solution

  ofstream file;


  if(_param.triangles_fname != "") {
    file.open(_param.triangles_fname);
    for(auto& edge: triangles_phase1) {
      file << solutions[edge.first]->costs.first << " ";
      file << solutions[edge.first]->costs.second << " ";
      file << solutions[edge.second]->costs.first << " ";
      file << solutions[edge.second]->costs.second << endl;
    }
    file.close();
  }

  if(_param.lb_fname != "") {
    file.open(_param.lb_fname);
    file << pareto_lb.ideal_costs.first << " " << pareto_lb.ideal_costs.second << endl;
    for(auto& edge: pareto_lb.edges_lowerbound) {
      file << edge.first.first << " " << edge.first.second << " ";
      file << edge.second.first << " " << edge.second.second << endl;
    }
    file.close();
  }
  

  if(_param.costs_fname != "") {
    file.open(_param.costs_fname);
    for(auto& sol: solutions) {
      file << sol->costs.first << " " << sol->costs.second << endl;
    }
    file.close();
  }

  if(_param.sol_fname != "") {

    file.open(_param.sol_fname);

    if(_param.all_solutions) {

      for(auto& sol: all_solutions_phase1) {
        for(auto& elt: sol->values) {
          file << elt.first << " " << elt.second << " ";
        }
        file << endl;
      }

      for(auto& sol: all_solutions_phase2) {
        for(auto& elt: sol->values) {
          file << elt.first << " " << elt.second << " ";
        }
        file << endl;
      }

    } else {
      for(auto& sol: solutions) {
        for(auto& elt: sol->values) {
          file << elt.first << " " << elt.second << " ";
        }
        file << endl;
      }
    }

    file.close();
  }

  assert(!complete_phase1 || !complete_phase2 || fabs((LB_area-UB_area) <= bicriteria::epsilon));

  if(_param.all_solutions) {
    assert(all_solutions_phase1.size()+all_solutions_phase2.size() >= solutions.size());
  }
  

  // file.open("solutions.txt");
  // for(shared_ptr<bicriteria::Solution> sol: solutions) {
  //   if(sol->getType() == bicriteria::Solution::Solution_Phase1) {
  //     shared_ptr<bicriteria::SolutionPhase1> sol_ptr = dynamic_pointer_cast<bicriteria::SolutionPhase1>(sol);
  //     file << sol->costs.first << " " << sol->costs.second << " " <<  sol_ptr->weights.first << " " << sol_ptr->weights.second << " " << sol_ptr->lb << endl;
  //   }
  // }
  // file.close();

  

}


//-------------------------------------------------------------------------
Double bicriteria::TwoPhases::computeUBArea(BiDouble bounds_c1, BiDouble bounds_c2, std::vector<std::shared_ptr<bicriteria::Solution>>& solutions) {

  // assuming solutions are sorted
  Double area = 0.;

  // cout << "bounds c1: " << bounds_c1 << endl;
  // cout << "bounds c2: " << bounds_c2 << endl;

  for(auto it = solutions.begin()+1; it != solutions.end(); it ++) {
    auto& prec = *(it-1);
    // cout << "two costs: prec and next: " << prec->costs << ", " << (*it)->costs << endl;
    // cout << "debug ub increment: " << (bounds_c1.second - (*it)->costs.first) * (prec->costs.second-(*it)->costs.second) << endl; 
    area += (bounds_c1.second - (*it)->costs.first) * (prec->costs.second-(*it)->costs.second);
  }
  area += (bounds_c1.second-solutions.front()->costs.first)*(bounds_c2.second-solutions.front()->costs.second);

  return area;
}
