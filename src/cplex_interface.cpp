#include "two_phases.hpp"

#ifdef ILOGCPLEX
#include <ilcplex/ilocplex.h>
#endif

#include "mcriteria/multicfn.hpp"
#include "solution.hpp"

using namespace std;

#ifdef ILOGCPLEX

ILOSTLBEGIN

//----------------------------------------------------------------------------
bool bicriteria::TwoPhases::solve_scalarization_CPLEX(MultiCFN& mcfn, SolutionPhase1& solution) {

  // mcfn.print(cout);
  // cout << endl << endl;

  n_solve_phase1 ++;

  // set the weights to multicfn
  mcfn.setWeight(0, solution.weights.first);
  mcfn.setWeight(1, solution.weights.second);

  IloEnv env;
  IloModel model(env);

  // list of cplex variables matching the multicfn variables
  vector<IloNumVarArray> domain_vars;
  vector<shared_ptr<IloNumVarArray>> tuple_vars(mcfn.cost_function.size(), nullptr);

  MultiCFN::ILP_encoding encoding = (_param.cplex_direct ? MultiCFN::ILP_Direct : MultiCFN::ILP_Tuple);
  
  vector<size_t> objectives;

  for(unsigned int netInd = 0; netInd < mcfn.nbNetworks(); netInd ++) {
    if(fabs(mcfn.getWeight(netInd) >= ToulBar2::epsilon)) {
      objectives.push_back(netInd);
    }
  }

  if(Parameters::verbose >= 1) {
    cout << "obj: " << objectives << endl;
    cout << "weights: " << solution.weights.first << ", " << solution.weights.second << endl;
  }

  vector<pair<size_t, pair<Double, Double>>> constraints; // no constraints in the first phase


  mcfn.makeIloModel(env, model, encoding, objectives, constraints, domain_vars, tuple_vars);

  IloCplex cplex(env);

  // verbosity
  if(Parameters::verbose == 0) {
    cplex.setOut(env.getNullStream());
  }
  cplex.setParam(IloCplex::Param::MIP::Display, Parameters::verbose);

  // CPX_PARAM_EPAGAP 0.00000000000000e+00   
  cplex.setParam(IloCplex::Param::MIP::Tolerances::AbsMIPGap, 0.00000000000000e+00); 
  cplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap, 0.00000000000000e+00);
  // CPX_PARAM_EPINT 0.00000000000000e+00   
  cplex.setParam(IloCplex::Param::MIP::Tolerances::Integrality, 0.00000000000000e+00); 
  // CPX_PARAM_EPRHS 1e-9
  cplex.setParam(IloCplex::Param::Simplex::Tolerances::Feasibility, 1e-9);
  // CPX_PARAM_THREADS 1
  cplex.setParam(IloCplex::Param::Threads, 1); 

  // time limit
  cplex.setParam(IloCplex::Param::TimeLimit, _param.timeout_phase1);

  // TODO choose the right epsilon, since obj <= ub (not obj < ub)
  // objective upper bound
  if(solution.ub_c1 != std::numeric_limits<Double>::infinity()) {
    // cout << "upper bound cplex: " << ub << endl;
    cplex.setParam(IloCplex::Param::MIP::Tolerances::UpperCutoff, solution.ub_c1-mcfn.getUnitCost());
  }

  MultiCFN::Solution sol_tb2;

  // // load toulbar2 solution
  // ifstream file("sol_toulbar2.txt");
  // while(!file.eof()) {
  //   string var, val;
  //   file >> var;
  //   file >> val;
  //   if(!file.eof()) {
  //     sol_tb2.insert(make_pair(var, val));
  //   }
  // }
  // file.close();

  // // variable assignment from toulbar2 solution
  // for(auto elt: sol_tb2) {
  //   int var_ind = mcfn.var_index[elt.first];
  //   mcriteria::Var& variable = mcfn.var[var_ind]; 
  //   int val_ind = variable.str_to_index[elt.second];
  //   if(variable.nbValues() > 2) {
  //     IloExpr expr(env);
  //     expr += domain_vars[var_ind][val_ind];
  //     model.add(expr == 1.);
  //     expr.end();
  //   } else if(variable.nbValues() == 2) {
  //     IloExpr expr(env);
  //     expr += domain_vars[var_ind][0];
  //     model.add(expr == val_ind);
  //     expr.end();
  //   }
  // }

  cplex.extract(model);

  cplex_aborter = std::make_shared<IloCplex::Aborter>(env);
  // cout << "aborter set up" << endl;

  // cplex.exportModel("cplexcpp.lp");

  /* res is true if a solution has been found*/
  // cout << "start solving" << endl;
  IloBool res = cplex.solve();

  solution.optimal = (cplex.getStatus() == IloAlgorithm::Optimal || cplex.getStatus() == IloAlgorithm::Infeasible);

  nb_nodes_phase1 += cplex.getNnodes();

  if(Parameters::verbose >= 1) {
    cplex.out() << "Cplex solution status: " << cplex.getStatus() << endl;
  }

  solution.lb = cplex.getBestObjValue();

  /* extract the solution */
  if(res && cplex.getObjValue() < solution.ub_c1) {

    if(Parameters::verbose >= 1) {
      cout << "Cplex found a solution" << endl;
    }

    // cout << "tuple variables: " << endl;
    // cout << std::setprecision(15);
    // size_t func_ind = 0;
    // for(auto& vec: tuple_vars) {
      
    //   if(vec != nullptr) {
        
    //     auto& func = mcfn.cost_function[func_ind];

    //     int cpt = 0;
    //     for(unsigned int ind = 0; ind < vec.get()->getSize(); ind ++) {
    //       try {
    //         Double cost = 0.;
    //         if(ind < func.costs.size()) {
    //           cost = func.costs[ind];
    //         } else {
    //           cost = func.default_cost;
    //         }
            
    //         Double val = cplex.getValue( (*(vec.get()))[ind] );
    //         if(val > 1e-4) {
    //           cpt ++;
    //         }
    //         if(val > 1e-4) {
    //           cout << "\033[31m";
    //         }
    //         cout << (*(vec.get()))[ind].getName() << "=" << val;
    //         // if(val > 1e-4) {
    //         cout << "(" << cost << ")";
    //         // }
    //         if(val > 1e-4) {
    //           cout << "\033[0m";
    //         }
    //         if(ind >= func.tuples.size()) { // additional tuple for default_cost
    //           cout << "**";
    //         }
    //         cout << ", ";
    //       } catch(IloException& ex) {
    //       }
    //     }
    //     cout << endl;
    //     if(cpt > 1) {
    //       cout << "error !!!!" << endl;
    //       // assert(false);
    //     }
    //     cout << endl << endl;
    //   }
    //   func_ind ++;
    // }

    mcfn.getCplexSolution(cplex, domain_vars, solution.values);

    auto costs = mcfn.computeSolutionValues(solution.values);

    assert(costs.size() == 2);

    solution.costs = make_pair(costs[0], costs[1]);

    solution.ub = cplex.getObjValue();
    if(solution.optimal) {
      solution.lb = cplex.getObjValue();
    }

    Double combi_recomputed = costs[0]*solution.weights.first + costs[1]*solution.weights.second;

    if(Parameters::verbose >= 2) {
      cout << "cplex optimum: " << cplex.getObjValue() << endl;
      cout << "mcfn optimum (from cplex sol): " << combi_recomputed << endl;
      cout << "cplex lower bound: " << solution.lb << endl;
    }

    // error: 
  

    // debug
    // cout << "mcfn truth: " << endl;
    // mcfn.outputNetSolutionCosts(0, solution.values);
    // cout << endl << endl;

    // cout << "cplex truth: " << endl;
    // mcfn.computeCriteriaSol(cplex, 0, false, domain_vars, tuple_vars);
    // cout << endl << endl;

    assert(fabs(combi_recomputed-solution.ub) <= 1e-3);

  } else {
    if(!res && Parameters::verbose >= 1) {
      cout << "no solution found by Cplex" << endl;
    }
    res = false;
    // for some reason, cplex may fail to cut out solutions higher than the specified upper bound :(
  }

  cplex_aborter = nullptr;

  return res;

}

//---------------------------------------------------------------------------
bool bicriteria::TwoPhases::solve_epsilon_CPLEX(MultiCFN& mcfn_total, bicriteria::SolutionPhase2& solution) {

  n_solve_phase2 ++;

  IloEnv env;
  IloModel model(env);

  // list of cplex variables matching the multicfn variables
  vector<IloNumVarArray> domain_vars;
  vector<shared_ptr<IloNumVarArray>> tuple_vars(mcfn_total.cost_function.size(), nullptr);

  MultiCFN::ILP_encoding encoding = (_param.cplex_direct ? MultiCFN::ILP_Direct : MultiCFN::ILP_Tuple);

  vector<size_t> objectives;
  objectives.push_back(0);
  if(_param.combine) { // add c2 to the objective function when the combination flag is activated
    objectives.push_back(1);
    mcfn_total.setWeight(0, solution.weights.first);
    mcfn_total.setWeight(1, solution.weights.second);
  }

  if(Parameters::verbose >= 2) {
    cout << "obj: " << objectives << endl;
    cout << "weights: " << solution.weights.first << ", " << solution.weights.second << endl;
  }

  // second criteria
  // lb_c2 <= c2 < ub_c2 
  vector<pair<size_t, BiDouble>> constraints;
  constraints.push_back(make_pair(1, make_pair(solution.bounds_c2.first+mcfn_total.getUnitCost(), solution.bounds_c2.second-mcfn_total.getUnitCost())));

  if(fabs(constraints.back().second.first-constraints.back().second.second) <= 1e-4) {
    constraints.back().second.first -= mcfn_total.getUnitCost()/2.;
    constraints.back().second.second += mcfn_total.getUnitCost()/2.;
  }

  if(Parameters::verbose >= 2) {
    cout << "cplex constraints: " << constraints.back().second << endl;
  }

  if(!_param.combine && constraints.back().second.second-constraints.back().second.first < mcfn_total.getUnitCost()) {
    if(Parameters::verbose >= 1) {
      cout << "Problem is infeasible due to the global constraint" << endl;
    }
    solution.optimal = true;
    return false;
  }

  mcfn_total.makeIloModel(env, model, encoding, objectives, constraints, domain_vars, tuple_vars);

  cplex_aborter = std::make_shared<IloCplex::Aborter>(env);

  IloCplex cplex(env);

  // verbosity
  if(Parameters::verbose == 0) {
    cplex.setOut(env.getNullStream());
  }
  cplex.setParam(IloCplex::Param::MIP::Display, Parameters::verbose);

  // CPX_PARAM_EPAGAP                 0.00000000000000e+00   
  cplex.setParam(IloCplex::Param::MIP::Tolerances::AbsMIPGap, 0.00000000000000e+00); 
  cplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap, 0.00000000000000e+00);
  // CPX_PARAM_EPINT                  0.00000000000000e+00   
  cplex.setParam(IloCplex::Param::MIP::Tolerances::Integrality, 0.00000000000000e+00); 
  // CPX_PARAM_EPRHS			1e-9
  cplex.setParam(IloCplex::Param::Simplex::Tolerances::Feasibility, 1e-9);
  // CPX_PARAM_THREADS                1
  cplex.setParam(IloCplex::Param::Threads, 1); 

  // time limit
  cplex.setParam(IloCplex::Param::TimeLimit, _param.timeout_phase2);

  // bounds on 1st criterion

  // TODO choose the right epsilon, since obj <= ub (not obj < ub)
  // objective upper bound
  BiDouble bounds_main;
  if(!_param.combine) {
    bounds_main = solution.bounds_c1;
  } else {
    bounds_main.first = solution.bounds_c1.first*solution.weights.first + solution.bounds_c2.first*solution.weights.second;
    bounds_main.second = solution.bounds_c1.second*solution.weights.first + solution.bounds_c2.second*solution.weights.second;
  }
  cplex.setParam(IloCplex::Param::Simplex::Limits::LowerObj, bounds_main.first); // LB main criterion (early stop)
  cplex.setParam(IloCplex::Param::MIP::Tolerances::UpperCutoff, bounds_main.second - mcfn_total.getUnitCost()); // UB main criterion

  cplex.extract(model);

  // debug dump
  // cplex.exportModel("cplexcpp.lp");


   /* res is true if a solution has been found*/
  IloBool res = cplex.solve();

  solution.optimal = (cplex.getStatus() == IloAlgorithm::Optimal || cplex.getStatus() == IloAlgorithm::Infeasible);

  nb_nodes_phase2 += cplex.getNnodes();

  if(Parameters::verbose >= 1) {
    cplex.out() << "cplex solution status: " << cplex.getStatus() << endl;
  }

  solution.lb = cplex.getBestObjValue();

  /* extract the solution */
  if(res) {

    if(Parameters::verbose >= 1) {
      cout << "Cplex found a solution" << endl;
    }

    mcfn_total.getCplexSolution(cplex, domain_vars, solution.values);

    auto costs = mcfn_total.computeSolutionValues(solution.values);

    assert(costs.size() == 2);

    solution.costs = make_pair(costs[0], costs[1]);

    solution.ub = cplex.getObjValue();
    // WARNING for some reason when the solution is optimal, the value returned by getBestObjValue() does not equal getObjValue()
    if(solution.optimal) {
      solution.lb = cplex.getObjValue();
    }

    Double combi_recomputed = costs[0]*solution.weights.first + costs[1]*solution.weights.second;

    if(Parameters::verbose >= 1) {
      cout << "cplex solution recomputed from multicfn: " << solution.costs << endl;
    }

    if(Parameters::verbose >= 2) {
      cout << "mcfn optimum = " << combi_recomputed << endl;
      cout << "cplex optimum: " << cplex.getObjValue() << endl;
    }

    // debugging
    // Double c2_cost = mcfn_total.computeCriteriaSol(cplex, 1, false, domain_vars, tuple_vars);
    // cout << "cost of c2 according to cplex: " << c2_cost << endl;

    // Double c1_cost = mcfn_total.computeCriteriaSol(cplex, 0, true, domain_vars, tuple_vars)+mcfn_total.computeCriteriaSol(cplex, 1, true, domain_vars, tuple_vars);
    // cout << "cost of c1 according to cplex: " << c1_cost << endl;

    assert(fabs(combi_recomputed-solution.ub) <= 1e-3);

    // cout << "mcfn optimum: " << costs[0]*solution.weights.first + costs[1]*solution.weights.second << endl;

  }

  cplex_aborter = nullptr;

  return res;

}

#endif
