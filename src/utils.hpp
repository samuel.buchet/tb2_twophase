#pragma once

#include <vector>

#include "toulbar2lib.hpp"
#include "mcriteria/bicriteria.hpp"

// random generation

//---------------------------------------------------------------------------
unsigned int random_int(unsigned int min, unsigned int max);

//---------------------------------------------------------------------------
double random_int(double min, double max);

//---------------------------------------------------------------------------
void init_global_scope(WeightedCSP* wcsp_1, WeightedCSP* wcsp_2, std::vector<int>& scope);

//---------------------------------------------------------------------------
bool set_tb2_extflags(const char* fileName);