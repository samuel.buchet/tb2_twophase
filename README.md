# Two Phases Bi-Objective Optimization Solver for Graphical Models

![3 pareto fronts computed by the solver](media/fig_pareto.svg "3 pareto fronts computed by the solver")

This program is a two-phases solver for bi-objective optimization of graphical models. It allows you to compute an exact anytime approximation of a pareto front of a bi-objective combinatorial problem formulated from two graphical models.
The method is based on solving scalarizations of the problem: this consists in solving multiple times a mono objective problem that includes the two objectives.

Two phases are performed:

* in the first phase, linear combinations of the two objectives are solved in order to compute the convex hull of the pareto front
* in the second phase, for each pair of consecutive points of the convex hull, a mono objective problem is solved with an additional constraint on the cost of the objectives, in order to find pareto-optimal solutions not located on the convex hull

This solver is implemented on top of the graphical model optimization solver [toulbar2](https://github.com/toulbar2/toulbar2/tree/cccrit), and can also be interfaced with the cplex solver through a dedicated encoding. Additional optimizers may also be interfaced with this solver.

## Installing

Executing `bash install.sh` will download the dependance, extract the instances and compile the program.
Use the option `-cplex` to test the program with cplex additionally to toulbar2.
If cplex is installed in a custom directory, please use the option `-cplex_dir dir` to specify it. See `bash install.sh -h` for options.

## Executing the program

List of the program options:

Input instances:
    
* **-cfn1 (string)** name of the first graphical model instance
* **-cfn2 (string)** name of the second graphical model instance

You can use any graphical model format known by [toulbar2](https://toulbar2.github.io/toulbar2/userdoc.html#file-output) solver (e.g. cfn, cnf, lp, opb, qpbo, uai, wbo, wcnf, wcsp). Both files must be in the same format. Variables having the same name (or position if no name given as in e.g., wcsp format) in the two instances are considered identical and they must share the same domain.

Time limit:

* **-global_timer (int)** time limit in seconds on the global execution of the program (3600sec by default)
* **-t (int)** time limit in seconds for each call to the single objective solver (30sec by default)
* **-m (int)** limit on the maximum number of calls to the single-objective solver (1000 by default)

Solver options:

* **-solver (string)**  solver to use for solving each scalarization. Options are **cplex** or **toulbar2** (option by default).
* **-cplex_encoding (string)** encoding that is used when solving with the cplex solver only. Options are **direct** and **tuple** (option by default).
* **-obj (string)** objective that is used as a constraint when performing the second phase. Options are combi (a weighted sum of the two objectives is used) and F1 (the first objective is used alone, option by default).
* **-knapsack** (toulbar2 solver only) when activated, the global constraint used during the second phase is transformed into a linear constraint (works only when the corresponding objective is composed exclusively of unary cost functions)
* **-bisupport (int)** (toulbar2 solver only) options for bounding the second objective during the second phase. For more details, please see [this page](https://toulbar2.github.io/toulbar2/userdoc.html#branching-variable-and-value-ordering)
* **-vac** (toulbar2 solver only) activate Virtual Arc Consistency in toulbar2
* **-epsilon** use the epsilon method instead of the two phase method to solve the instance
* **-allsol** after finishing solving the problem, enumerate all the soutions corresponding to the cost vectors of the pareto front found (available only when the problem is solved to optimality)

Output options:

* **-v (int)** verbosity level, from 0 to 3 (0 by default)
* **-fcosts (string)** file name for the exportation of the list of solution cost vectors
* **-flb (string)**  file name for the exportation of the list of the dual bound on the pareto front
* **-ftriangles (string)**  file name for the exportation of the list of the triangles computed in phase 1
* **-fsol (string)**  file name for the exportation of the list of the output solutions

Example: 

Solving and exporting the output (solutions, upper bound, lower bound and phase 1 triangles): `./main -cfn1 instances/setcoversc/fixed-set-card-200-20-10-3_1.cfn -cfn2 instances/setcoversc/fixed-set-card-200-20-10-3_2.cfn -m 1000 -t 1 -fcosts costs.txt -flb lb.txt -ftriangles triangles.txt -v 1`

Visualizing the pareto fronts:

A visualisation tool is available in `utils/visualisation.py` to make a graph of the pareto front for each instance. Example:

`python utils/visualisation.py -c costs.txt -lb lb.txt -t triangles.txt -i`

![A pareto front of a partially solved instance](media/pareto_example.svg "A pareto front of a partially solved instance")

See `python utils/visualisation.py -h` for more details.

## Benchmarks

Benchmarks have been performed on multiple instances to compare the use of toulbar2 and cplex for the scalarizations. The results of these experiments (csv tables) are available in the folders results/expe_tb2 and results/expe_cplex. It is also possible to generate the pareto front plots with the scripts `results/expe_{cplex/tb2}/make_graphs.sh`.
Supplementary information about these experiments is available in the pdf file `results/supplementals.pdf`.

Benchmarks can be run again with the bash scripts in the `benchmarks` folder (run it from the root directory): `bash benchmarks/run_all_tb2.sh` and `bash benchmarks/run_all_cplex.sh`.
