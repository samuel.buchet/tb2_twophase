#!/bin/bash

shopt -s nullglob

make clean

rm -rf dep/toulbar2 2> /dev/null
rm -r results/expe_cplex 2> /dev/null
rm -r results/expe_tb2 2> /dev/null


for elt in instances/*/
do
    rm -r "$elt" 2> /dev/null
done

for elt in benchmarks/*/
do
    rm -r "$elt" 2> /dev/null
done

for elt in benchmarks/*.txt
do
    rm -r "$elt" 2> /dev/null
done

rm instances/proteins_biobj.zip 2> /dev/null
