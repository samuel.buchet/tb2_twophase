#!/bin/bash

# toulbar2 experiments
mkdir benchmarks/expe_tb2 &> /dev/null



#########################################
# bivertex cover
#########################################

# bivertexcover
mkdir benchmarks/expe_tb2/bivertexcover &> /dev/null
bash benchmarks/run_tb2.sh instances/bivertexcover benchmarks/expe_tb2/bivertexcover '_hard' 30 1 1 > benchmarks/expe_tb2/bivertexcover.txt

# bivertexcovernoknapsack
mkdir benchmarks/expe_tb2/bivertexcovernoknapsack &> /dev/null
bash benchmarks/run_tb2.sh instances/bivertexcover benchmarks/expe_tb2/bivertexcovernoknapsack '_hard' 30 0 1  > benchmarks/expe_tb2/bivertexcovernoknapsack.txt

# bivertexcovernovac
mkdir benchmarks/expe_tb2/bivertexcovernovac &> /dev/null
bash benchmarks/run_tb2.sh instances/bivertexcover benchmarks/expe_tb2/bivertexcovernovac '_hard' 30 1 0 > benchmarks/expe_tb2/bivertexcovernovac.txt

# bivertexcovernovacnoknapsack
mkdir benchmarks/expe_tb2/bivertexcovernovacnoknapsack &> /dev/null
bash benchmarks/run_tb2.sh instances/bivertexcover benchmarks/expe_tb2/bivertexcovernovacnoknapsack '_hard' 30 0 0 > expe_tb2/bivertexcovernovacnoknapsack.txt 



#########################################
# protein design
#########################################

# proteins
mkdir benchmarks/expe_tb2/proteins &> /dev/null
bash benchmarks/run_tb2.sh instances/proteins benchmarks/expe_tb2/proteins '' 300 1 1 > benchmarks/expe_tb2/proteins.txt

# proteinsnoknapsack
mkdir benchmarks/expe_tb2/proteinsnoknapsack &> /dev/null
bash benchmarks/run_tb2.sh instances/proteins benchmarks/expe_tb2/proteinsnoknapsack '' 300 0 1  > benchmarks/expe_tb2/proteinsnoknapsack.txt

# proteinsnovac
mkdir benchmarks/expe_tb2/proteinsnovac &> /dev/null
bash benchmarks/run_tb2.sh instances/proteins benchmarks/expe_tb2/proteinsnovac '' 300 1 0 > benchmarks/expe_tb2/proteinsnovac.txt

# proteinsnovacnoknapsack
mkdir benchmarks/expe_tb2/proteinsnovacnoknapsack &> /dev/null
bash benchmarks/run_tb2.sh instances/proteins benchmarks/expe_tb2/proteinsnovacnoknapsack '' 300 0 0 > benchmarks/expe_tb2/proteinsnovacnoknapsack.txt


#########################################
# facility location
#########################################

# FLP
mkdir benchmarks/expe_tb2/FLP/ &> /dev/null
bash benchmarks/run_tb2.sh instances/FLP benchmarks/expe_tb2/FLP '_hard' 30 1 1 > benchmarks/expe_tb2/FLP.txt

# FLPnoknapsack
mkdir benchmarks/expe_tb2/FLPnoknapsack/ &> /dev/null
bash benchmarks/run_tb2.sh instances/FLP benchmarks/expe_tb2/FLPnoknapsack '_hard' 30 0 1 > benchmarks/expe_tb2/FLPnoknapsack.txt

# FLPnovac
mkdir benchmarks/expe_tb2/FLPnovac/ &> /dev/null
bash benchmarks/run_tb2.sh instances/FLP benchmarks/expe_tb2/FLPnovac '_hard' 30 1 0 > benchmarks/expe_tb2/FLPnovac.txt

# FLPnovacnoknapsack
mkdir benchmarks/expe_tb2/FLPnovacnoknapsack/ &> /dev/null
bash benchmarks/run_tb2.sh instances/FLP benchmarks/expe_tb2/FLPnovacnoknapsack '_hard' 30 0 0 > benchmarks/expe_tb2/FLPnovacnoknapsack.txt



#########################################
# set cover
#########################################

# setcoversc
mkdir benchmarks/expe_tb2/setcoversc &> /dev/null
bash benchmarks/run_tb2.sh instances/setcoversc benchmarks/expe_tb2/setcoversc '' 3600 1 1 > benchmarks/expe_tb2/setcoversc.txt

# setcoverscnovac
mkdir benchmarks/expe_tb2/setcoverscnovac &> /dev/null
bash benchmarks/run_tb2.sh instances/setcoversc benchmarks/expe_tb2/setcoverscnovac '' 3600 1 0 > benchmarks/expe_tb2/setcoverscnovac.txt


#########################################
# uai2022
#########################################

# uai2022mmapnopre
mkdir benchmarks/expe_tb2/uai2022mmapnopre &> /dev/null
bash benchmarks/run_tb2.sh instances/uai2022mmap benchmarks/expe_tb2/uai2022mmapnopre '_hard' 30 1 1 > benchmarks/expe_tb2/uai2022mmapnopre.txt

# uai2022mmappre
mkdir benchmarks/expe_tb2/uai2022mmappre &> /dev/null
bash benchmarks/run_tb2.sh instances/uai2022mmap benchmarks/expe_tb2/uai2022mmappre '_pre' 30 1 1 > benchmarks/expe_tb2/uai2022mmappre.txt

# uai2022mmapprenovac
mkdir benchmarks/expe_tb2/uai2022mmapprenovac &> /dev/null
bash benchmarks/run_tb2.sh instances/uai2022mmap benchmarks/expe_tb2/uai2022mmapprenovac '_pre' 30 1 0 > benchmarks/expe_tb2/uai2022mmapprenovac.txt

#########################################
# knapsack
#########################################

# knapsack
mkdir benchmarks/expe_tb2/knapsack &> /dev/null
bash benchmarks/run_tb2.sh instances/knapsack benchmarks/expe_tb2/knapsack '' 30 1 0 > benchmarks/expe_tb2/knapsack.txt

