#!/bin/bash

# cplex experiments
mkdir benchmarks/expe_cplex &> /dev/null

# bivertexcover/
mkdir benchmarks/expe_cplex/bivertexcover &> /dev/null
bash benchmarks/run_cplex.sh instances/bivertexcover benchmarks/expe_cplex/bivertexcover '' 30 > benchmarks/expe_cplex/bivertexcover.txt

# setcoversc3600/
mkdir benchmarks/expe_cplex/setcoversc &> /dev/null
bash benchmarks/run_cplex.sh instances/setcoversc benchmarks/expe_cplex/setcoversc '' 3600 > benchmarks/expe_cplex/setcoversc.txt


# FLP/
mkdir benchmarks/expe_cplex/FLP &> /dev/null
bash benchmarks/run_cplex.sh instances/FLP benchmarks/expe_cplex/FLP '' 30 > benchmarks/expe_cplex/FLP.txt

# knapsack/
mkdir benchmarks/expe_cplex/knapsack &> /dev/null
bash benchmarks/run_cplex.sh instances/knapsack benchmarks/expe_cplex/knapsack '' 30 > benchmarks/expe_cplex/knapsack.txt


# uai2022mmap300
mkdir benchmarks/expe_cplex/uai2022mmap300 &> /dev/null
bash benchmarks/run_cplex.sh instances/uai2022mmap benchmarks/expe_cplex/uai2022mmap300 '' 300 > benchmarks/expe_cplex/uai2022mmap300.txt


# uai2022mmap30
mkdir benchmarks/expe_cplex/uai2022mmap30 &> /dev/null
bash benchmarks/run_cplex.sh instances/uai2022mmap benchmarks/expe_cplex/uai2022mmap30 '' 30 > benchmarks/expe_cplex/uai2022mmap30.txt


# proteins
mkdir benchmarks/expe_cplex/proteins &> /dev/null
bash benchmarks/run_cplex.sh instances/proteins benchmarks/expe_cplex/proteins '' 300 > benchmarks/expe_cplex/proteins.txt
