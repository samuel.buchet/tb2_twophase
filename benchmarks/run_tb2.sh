#!/bin/bash

# $1: input_dir 
# $2 output_dir
# $3 suffix
# $4 t (time limit)
# $5 knapsack ? 1 or 0
# $6 vac ? 1 or 0

input_dir=$1
output_dir=$2

knapsack=''
if [ $5 -eq 1 ]; then
   knapsack='-knapsack'
fi

vac=''
if [ $6 -eq 1 ]; then
   vac='-vac'
fi

# echo "vac ? $vac"
# echo "knapsack ? $knapsack"


for f in $1/*_1.cfn
do
   f="${f/$input_dir/}"
   f="${f/'_1.cfn'/}"   

   ./main -cfn1 "$input_dir$f"_1"$3".cfn -cfn2 "$input_dir$f"_2"$3".cfn -obj F1 $knapsack $vac -bisupport -3 -m 1000 -t $4 -solver toulbar2 -cplex_encoding tuple -fcosts "$output_dir$f$3"_costs.txt -flb "$output_dir"$f$3_lb.txt -ftriangles "$output_dir$f$3"_triangles.txt -fsol "$output_dir$f$3"_solutions.txt

done
