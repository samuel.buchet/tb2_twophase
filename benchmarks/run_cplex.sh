#!/bin/bash

# $1: input_dir 
# $2 output_dir
# $3 suffix
# $4 t (time limit)

input_dir=$1
output_dir=$2

for f in $1/*_1.cfn
do
   f="${f/$input_dir/}"
   f="${f/'_1.cfn'/}"   

   ./main -cfn1 "$input_dir$f"_1"$3".cfn -cfn2 "$input_dir$f"_2"$3".cfn -obj F1 -knapsack -bisupport -3 -m 1000 -t $4 -solver cplex -cplex_encoding tuple -fcosts "$output_dir$f$3"_costs_new.txt -flb "$output_dir"$f$3_lb.txt -ftriangles "$output_dir$f$3"_triangles.txt -fsol "$output_dir$f$3"_solutions.txt

done
